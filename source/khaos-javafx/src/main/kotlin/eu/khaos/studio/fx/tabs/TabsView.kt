package eu.khaos.studio.fx.tabs

import de.saxsys.mvvmfx.*
import eu.khaos.studio.fx.environment.EnvironmentView
import eu.khaos.studio.fx.wsdlrequest.WsdlRequestView
import eu.khaos.studio.fx.wsdlrequest.taskbar.TaskbarView
import javafx.fxml.Initializable
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import java.net.URL
import java.util.*

class TabsView : TabPane(), JavaView<TabsViewModel> {


    @InjectViewModel
    private var model: TabsViewModel? = null

    init {
        val tab = Tab("test")
        tab.content = Label("abc")

        tabs.add(tab)
    }

    fun initialize() {
        model!!.openRequest = this::openRequest
        model!!.openEnvironment = this::openEnvironments

    }

    fun openRequest(name: String, scopes: List<Scope>) {

        val wsdlRequestView = FluentViewLoader.javaView(WsdlRequestView::class.java)
                .providedScopes(scopes)
                .load()

        addTab(name, wsdlRequestView.view)
    }

    fun openEnvironments(name: String, scopes: List<Scope>) {

        val environmentView = FluentViewLoader.javaView(EnvironmentView::class.java)
                .providedScopes(scopes)
                .load()
        addTab(name, environmentView.view)
    }

    private fun addTab(name: String, content: Node) {
        val newTab = Tab(name)
        newTab.content = content
        tabs.add(newTab)
        selectionModel.select(newTab)
    }
}

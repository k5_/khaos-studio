package eu.khaos.storage.file.model

import java.nio.file.Path
import javax.xml.bind.annotation.*

@XmlRootElement(name = "storage")
@XmlAccessorType(XmlAccessType.NONE)
class FileProject {

    @XmlElement(name = "name")
    var name: String? = null

    @XmlElementWrapper(name = "wsdls")
    @XmlElement(name = "wsdl")
    var wsdls: ArrayList<FileWsdl> = ArrayList()


    @XmlTransient
    var path: Path?=null
}



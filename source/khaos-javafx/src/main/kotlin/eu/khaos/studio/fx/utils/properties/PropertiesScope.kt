package eu.khaos.studio.fx.utils.properties

import de.saxsys.mvvmfx.Scope
import eu.khaos.studio.base.CTProperties
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty

class PropertiesScope(
        val properties: ObjectProperty<CTProperties>,
        val baseProperties: ObjectProperty<List<CTProperties>>

) : Scope {
    constructor() : this(SimpleObjectProperty(), SimpleObjectProperty())
}
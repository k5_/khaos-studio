package eu.khaos.studio.fx.navigation.dialog

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty

class CreateDialogModel {

    val name = SimpleStringProperty()


    val folderClicked = SimpleBooleanProperty(false)

    val requestClicked = SimpleBooleanProperty(false)

    val testcaseClicked = SimpleBooleanProperty(false)
}
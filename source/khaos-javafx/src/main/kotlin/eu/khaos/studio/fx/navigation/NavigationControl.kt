package eu.khaos.studio.fx.navigation

import eu.khaos.storage.*
import eu.khaos.studio.client.KhaosClient
import eu.khaos.studio.fx.KhaosAppModel
import eu.khaos.studio.fx.MessageBus
import eu.khaos.studio.fx.OpenItemEvent
import eu.khaos.studio.fx.navigation.dialog.CreateDialog
import javafx.scene.Node

class NavigationControl(val cmodel: KhaosAppModel) {
    private val model = NavigationModel()
    private val view = NavigationView(model)

    val client = KhaosClient("http://localhost:8080/api")
    val storage = client.storage()

    init {
        init()
    }

    fun init() {
        val tree = storage.list()

        addItemList(model.root, tree?.items.orEmpty())

/*        {
            val projectItem = ProjectItem("")
            projectItem.name.set(item.name)
            projectItem.type.set(ProjectItemType.PROJECT)
            projectItem.path.set(item.path)
            model.items.add(projectItem)
        }*/


        model.addItem.addListener { _, _, _ -> createItem() }
        model.selected.addListener { _, _, newValue -> selectionChanged(newValue) }
        model.openItem.addListener { _, _, newValue -> openItem(newValue) }
    }

    fun asNode(): Node {
        return view.asNode()
    }

    fun createItem() {

        val selected = model.selected.get()
        if (selected == null) {
            return
        }
        val dialog = CreateDialog(selected)

        dialog.requestCreateAction = { createItem(dialog.parent, it.name.get(), ItemType.REQUEST) }
        dialog.folderCreateAction = { createItem(dialog.parent, it.name.get(), ItemType.FOLDER) }
        dialog.testcaseCreateAction = { createItem(dialog.parent, it.name.get(), ItemType.TESTCASE) }

        dialog.display()
    }

    fun createItem(selected: ProjectItem, name: String, type: ItemType) {
        val project = client.project(selected.project.get())
        val addItem = AddItemRequest()
        addItem.name = name
        addItem.type = type
        if (ProjectItemType.FOLDER == selected.type.get()) {
            addItem.parent = ItemRef()
            addItem.parent!!.id = selected.id.get()
        }
        project.createItem(addItem)
    }


    fun openItem(projectItem: ProjectItem?) {
        if (projectItem == null) {
            return
        }
        if (projectItem.type.get() == ProjectItemType.REQUEST) {
            MessageBus.fire(OpenItemEvent(projectItem.name.get(), projectItem.project.get(), projectItem.path.get(), projectItem.type.get()))
        }
    }

    fun selectionChanged(projectItem: ProjectItem?) {
        if (projectItem == null) {
            return
        }
        if (projectItem.type.get() == ProjectItemType.REQUEST) {
            cmodel.currentItem.set(projectItem)
        } else if (projectItem.type.get() == ProjectItemType.PROJECT) {
            val project = client.project(projectItem.path.get()).list()

            addItemList(projectItem, project.items!!)

        }
    }

    private fun addItemList(addRoot: ProjectItem, items: List<ItemRef>) {
        for (newItem in items.orEmpty()) {
            println(newItem.path)
            if (newItem.id!!.contains("!")) {
                val pathParts = newItem.id!!.split("!")

                var root: ProjectItem? = addRoot
                for ((index, part) in pathParts.withIndex()) {
                    val child = root!!.getChild(part)
                    if (child != null) {
                        root = child
                    } else if (index == pathParts.size - 1) {
                        root = addItem(root, newItem, part)
                    } else {
                        root = addFolder(root, part)
                    }
                }
            } else {
                addItem(addRoot, newItem, newItem.id!!)
            }
        }
    }

    private fun addFolder(projectItem: ProjectItem, part: String): ProjectItem {
        val item = ProjectItem(part)
        item.type.set(ProjectItemType.FOLDER)
        item.name.set(part)
        projectItem.children.add(item)
        return item

    }


    private fun addItem(projectItem: ProjectItem, newItem: ItemRef, localPath: String): ProjectItem? {
        if (!contains(projectItem, localPath)) {
            val item = ProjectItem(localPath)
            item.id.set(newItem.id)
            item.name.set(newItem.name)
            item.path.set(newItem.path)
            item.project.set(projectItem.project.get())
            if (newItem.type == ItemType.REQUEST) {
                item.type.set(ProjectItemType.REQUEST)
            } else if (newItem.type == ItemType.PROJECT) {
                item.type.set(ProjectItemType.PROJECT)
                item.project.set(newItem.path)
            } else if (newItem.type == ItemType.FOLDER) {
                item.type.set(ProjectItemType.FOLDER)
            }
            projectItem.children.add(item)
            return item
        } else {
            return projectItem.getChild(localPath)
        }
    }

    private fun contains(projectItem: ProjectItem, path: String): Boolean {
        for (child in projectItem.children) {
            if (child.localPath == path) {
                return true
            }
        }
        return false
    }
}
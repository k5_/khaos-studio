package eu.khaos.storage.file.boundary

import eu.khaos.storage.*
import eu.khaos.storage.file.control.FileModel
import eu.khaos.storage.file.control.FileSanitizer
import eu.khaos.storage.file.model.*

import eu.khaos.storage.request.RequestResource
import eu.khaos.storage.wsdl.AddWsdl
import java.io.IOException
import java.nio.file.FileVisitResult
import java.nio.file.FileVisitor
import java.nio.file.Files.walkFileTree
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes
import java.util.*
import javax.xml.bind.JAXB


class ProjectResouceBean(
        val project: FileProjectRef,
        val repository: RepositoryResourceBean
) : ProjectResource {



    private val sanitize = FileSanitizer()
    private val model = FileModel()

    override fun list(): ProjectTree {
        val tree = ProjectTree()
        val projectRef = ItemRef()
        projectRef.path = project.self

        walkFileTree(project.filePath.parent, ProjectTreeVisitor(tree, projectRef, model))
        return tree
    }

    private class ProjectTreeVisitor(val tree: ProjectTree, val projectRef: ItemRef, val model: FileModel) : FileVisitor<Path> {

        private val path = ArrayDeque<String>()


        override fun preVisitDirectory(dir: Path?, attrs: BasicFileAttributes?): FileVisitResult {
            path.push(dir?.fileName?.toString())
            return FileVisitResult.CONTINUE
        }

        override fun postVisitDirectory(dir: Path?, exc: IOException?): FileVisitResult {
            path.pop()
            return FileVisitResult.CONTINUE
        }

        override fun visitFileFailed(file: Path?, exc: IOException?): FileVisitResult {
            return FileVisitResult.CONTINUE
        }


        override fun visitFile(file: Path, attrs: BasicFileAttributes?): FileVisitResult {
            if ("request.xml" == file!!.fileName.toString()) {
                val fileRequest = model.fromXml(file!!, FileRequest::class.java)
                val ref = ItemRef()
                ref.name = fileRequest.name
                ref.path = projectRef.path + "/request/" + path.reversed().subList(1, path.size).joinToString("!")
                ref.id = path.reversed().subList(1, path.size).joinToString("!")
                ref.parent = projectRef
                ref.type = ItemType.REQUEST
                tree.items!!.add(ref)
            } else if ("folder.xml" == file.fileName.toString()) {
                val fileFolder = model.fromXml(file, FileFolder::class.java)
                val ref = ItemRef()
                ref.name = fileFolder.name
                ref.path = projectRef.path + "/folder/" + path.reversed().subList(1, path.size).joinToString("!")
                ref.id = path.reversed().subList(1, path.size).joinToString("!")
                ref.parent = projectRef
                ref.type = ItemType.FOLDER
                tree.items!!.add(ref)
            }
            return FileVisitResult.CONTINUE
        }
    }

    override fun request(requestId: String): RequestResource {
        val requestPath: Path = resolvePath(requestId)
        return RequestResourceBean(FileRequestRef(project, requestId, requestPath))
    }
    override fun testcase(testcaseId: String): TestcaseResource {
        val testcasePath = resolvePath(testcaseId)
        return TestcaseResourceBean(FileItemRef(project, testcaseId, testcasePath))
    }


    override fun createItem(addItem: AddItemRequest): ItemRef {
        val id = sanitize.createId(addItem.name!!, addItem.parent)

        val itemPath = resolvePath(id)

        val type = addItem.type!!

        val path = if (type == ItemType.FOLDER) {
            createFolder(itemPath, addItem.name!!)
        } else if (type == ItemType.REQUEST) {
            createRequest(itemPath, addItem.name!!)
        } else if (type == ItemType.TESTCASE) {
            createTestcase(itemPath, addItem.name!!)
        } else {
            throw IllegalArgumentException("Unsupported ItemType " + type.name)
        }

        val item = ItemRef()
        item.id = id
        item.name = addItem.name
        item.type = type
        item.parent = item.parent
        item.path = project.self + path + id
        return item
    }

    private fun createFolder(path: Path, name: String): String {
        val file = path.resolve("folder.xml")
        val fileFolder = FileFolder()
        fileFolder.name = name
        model.write(file, fileFolder)
        return "/folder/"
    }

    private fun createRequest(path: Path, name: String): String {
        val file = path.resolve("request.xml")
        val fileRequest = FileRequest()
        fileRequest.name = name
        model.write(file, fileRequest)
        return "/request/"
    }


    private fun createTestcase(path: Path, name: String): String {
        val file = path.resolve("testcase.xml")
        val fileTestcase = FileTestcase()
        fileTestcase.name = name
        model.write(file, fileTestcase)
        return "/testcase/"
    }


    override fun addWsdl(addInterface: AddWsdl) {
        val fileProject = loadFileProject(project)

        val fileWsdl = FileWsdl()
        fileWsdl.name = addInterface.name
        fileProject.wsdls.add(fileWsdl)

        updateFileProject(fileProject)
    }


    override fun get(): Project {
        val fileProject = loadFileProject(project)

        val project = Project()
        project.name = fileProject.name
        project.self = this.project.self!!
        project.projectId = this.project.projectId!!
        return project
    }

    private fun loadFileProject(project: FileProjectRef): FileProject {
        val fileProject = JAXB.unmarshal(project.filePath.toFile(), FileProject::class.java)
        fileProject.path = project.filePath
        return fileProject
    }


    private fun updateFileProject(fileProject: FileProject) {
        val path: Path = fileProject.path ?: throw IllegalStateException("FileProject path not set")

        JAXB.marshal(fileProject, path.toFile())
    }

    private fun resolvePath(itemId: String): Path {
        if (!itemId.contains('!')) {
            return project.filePath.parent.resolve(itemId)
        } else {
            var folder = project.filePath.parent
            for (part in itemId.split("!")) {
                folder = folder.resolve(part)
            }
            return folder
        }
    }

}
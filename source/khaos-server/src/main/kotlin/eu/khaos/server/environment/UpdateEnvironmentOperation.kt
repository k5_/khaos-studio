package eu.khaos.server.environment

import eu.khaos.studio.environment.*
import javax.inject.Inject

class UpdateEnvironmentOperation @Inject constructor(
        private val repository: EnvironmentRepository
) : AbstractOperation<CTUpdateEnvironmentRequest, CTUpdateEnvironmentResponse>(
        ObjectFactory()::createCTUpdateEnvironmentResponse
) {


    override fun invoke(request: CTUpdateEnvironmentRequest): CTUpdateEnvironmentResponse {
        val update = request.environment

        val current = repository.getByKey(update.key)

        if (current == null) {
            throw RuntimeException()
        }
        current.name = update.name
        current.host = update.host

        val ctResponse = success()
        ctResponse.environment.add(current)
        return ctResponse
    }


}
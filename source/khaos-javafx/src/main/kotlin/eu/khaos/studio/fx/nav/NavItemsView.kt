package eu.khaos.studio.fx.nav

import de.saxsys.mvvmfx.InjectViewModel
import de.saxsys.mvvmfx.JavaView
import eu.khaos.studio.fx.Icons
import eu.khaos.studio.fx.navigation.ItemsNavView
import eu.khaos.studio.fx.navigation.ProjectItem
import eu.khaos.studio.fx.utils.Bind
import javafx.application.Platform
import javafx.collections.ListChangeListener
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.TitledPane
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox

class NavItemsView : TitledPane(), JavaView<NavItemsViewModel> {

    private val main = VBox()

    private val actionbar = HBox()

    private val projectsRoot = TreeItem<ProjectItem>()

    private val projects = TreeView<ProjectItem>(projectsRoot);

    @InjectViewModel
    private var model: NavItemsViewModel? = null

    init {
        text = "Projects"
        val addButton = Button("", Icons.ADD.get())

        addButton.onAction = EventHandler { addItem() }
        actionbar.children.addAll(addButton)

        projects.isShowRoot = false

        main.children.addAll(actionbar, projects)

        content = main
    }

    fun addItem() {

    }

    fun initialize() {
        addItem(projectsRoot, model!!.items)
        model!!.items.addListener(ItemsNavView.MyListChangeListener(projectsRoot))


        model!!.selected.bind(Bind.map(projects.selectionModel.selectedItemProperty()) { it.value })

        projects.onMouseClicked = EventHandler {
            if (it.button == MouseButton.PRIMARY && it.clickCount == 2) {
                model!!.openItem()
            }
        }
    }


    fun addItem(rootNode: TreeItem<ProjectItem>, addedItems: List<ProjectItem>) {
        for (added in addedItems) {
            val icon: Node? = Icons.getByItemType(added.type.get())
            val item = TreeItem(added, icon)
            added.children.addListener(NewItemsChangeListener(item))
            rootNode.children.add(item)
        }
    }

    inner class NewItemsChangeListener(private val rootNode: TreeItem<ProjectItem>) : ListChangeListener<ProjectItem> {
        override fun onChanged(c: ListChangeListener.Change<out ProjectItem>?) {
            if (c == null) {
                return
            }
            val addedList = ArrayList<TreeItem<ProjectItem>>()

            while (c.next()) {
                if (c.wasAdded()) {
                    addItem(rootNode, c.addedSubList)
                }
            }
            Platform.runLater {
                rootNode.children.addAll(addedList)
            }
        }
    }

}
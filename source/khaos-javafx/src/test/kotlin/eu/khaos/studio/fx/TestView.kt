package eu.khaos.studio.fx

import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.InjectViewModel
import de.saxsys.mvvmfx.JavaView
import eu.k5.arch.menu.MenuView
import eu.khaos.studio.base.CTProperties
import eu.khaos.studio.base.CTProperty
import eu.khaos.studio.environment.CTEnvironment
import eu.khaos.studio.fx.environment.EditEnvironmentView
import eu.khaos.studio.fx.environment.EnvironmentScope
import eu.khaos.studio.fx.utils.properties.PropertiesScope
import eu.khaos.studio.fx.utils.properties.PropertiesView
import javafx.scene.Parent
import javafx.scene.control.Label
import javafx.scene.control.SplitPane
import javafx.scene.layout.VBox

class TestView : VBox(), JavaView<TestViewModel> {

    val splitPane = SplitPane()

    @InjectViewModel
    private var model: TestViewModel? = null

    init {
        //    children.add(Label("abc"))
    }

    fun initialize() {
        //       val main = FluentViewLoader.javaView(MenuView::class.java).load()

        //val view = loadProperties()
        val view = loadEditEnvironment()
        this.children.add(view)
    }

    private fun loadEditEnvironment(): Parent {

        val scope = EnvironmentScope()

        val ctEnvironment = CTEnvironment()
        ctEnvironment.name = "TestStart Evn"
        ctEnvironment.host = "http://localhost:8080/"

        scope.environment.set(ctEnvironment)


        val main = FluentViewLoader.javaView(EditEnvironmentView::class.java).providedScopes(scope).load()

        return main.view
    }

    private fun loadProperties(): Parent {

        val properties = CTProperties()
        val property = CTProperty()
        property.key = "test"
        property.value = "me"
        properties.property.add(property)

        val scope = PropertiesScope()
        scope.properties.set(properties)


        val main = FluentViewLoader.javaView(PropertiesView::class.java).providedScopes(scope).load()

        return main.view
    }
}
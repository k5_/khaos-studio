package eu.khaos.studio.fx

@FunctionalInterface
interface Action {
    fun act() : Unit
}
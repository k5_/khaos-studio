package eu.khaos.storage

class AddItemRequest {

    var name: String? = null
    var parent: ItemRef? = null

    var type: ItemType? = null
}

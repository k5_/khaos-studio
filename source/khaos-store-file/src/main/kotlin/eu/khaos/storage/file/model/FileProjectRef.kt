package eu.khaos.storage.file.model

import java.nio.file.Path

class FileProjectRef(
        val self: String,
        val projectId: String,
        val filePath: Path
)
package eu.khaos.studio.fx

import javafx.application.Application
import javafx.stage.Stage


fun main(args: Array<String>) {
    Application.launch(FxApplication::class.java)
}

class FxApplication : Application() {
    override fun start(primaryStage: Stage) {
        primaryStage.title = "Khaos Studio"
        val model = KhaosAppModel()
        val view = KhaosAppView(model)
        primaryStage.scene = view.asScene()
        primaryStage.show()
    }

}
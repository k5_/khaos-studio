package eu.khaos.studio.client

class ClientException(message: String, val code: String) : RuntimeException(message) {


    class ErrorEntity {
        var message: String? = null
        var code: String? = null
    }
}
package eu.k5.arch.request

import de.saxsys.mvvmfx.InjectViewModel
import de.saxsys.mvvmfx.JavaView
import de.saxsys.mvvmfx.ViewModel
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import java.net.URL
import java.util.*

class TaskbarView : HBox(), JavaView<TaskbarViewModel>, Initializable {

    val start = Button("start")
    val stop = Button("stop")
    val input = TextField()
    val save = Button("save")


    @InjectViewModel
    var model: TaskbarViewModel? = null

    init {
        children.addAll(start, stop, input, save)
    }

    override fun initialize(location: URL?, resources: ResourceBundle?) {

        input.textProperty().set(model!!.scope!!.requestProperty().get().content)


    }


}
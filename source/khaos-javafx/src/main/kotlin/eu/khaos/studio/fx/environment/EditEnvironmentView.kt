package eu.khaos.studio.fx.environment

import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.InjectViewModel
import de.saxsys.mvvmfx.JavaView
import eu.khaos.studio.fx.utils.properties.PropertiesView
import javafx.geometry.Insets
import javafx.scene.control.Label
import javafx.scene.control.Separator
import javafx.scene.control.TextField
import javafx.scene.layout.ColumnConstraints
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox

class EditEnvironmentView : VBox(), JavaView<EditEnvironmentViewModel> {

    @InjectViewModel
    private var model: EditEnvironmentViewModel? = null

    private val grid = GridPane()

    private val nameField = TextField()

    private val hostField = TextField()


    init {
        this.isFillWidth = true


        val label = ColumnConstraints(75.0)
        label.hgrow = Priority.SOMETIMES

        val c = ColumnConstraints(75.0)
        c.hgrow = Priority.ALWAYS

        grid.columnConstraints.addAll(label, c)

        grid.add(Label("Name"), 0, 0)
        grid.add(nameField, 1, 0)

        grid.add(Label("Host"), 0, 1)
        grid.add(hostField, 1, 1)

        val separator = Separator()
        GridPane.setColumnSpan(separator, 4)
        grid.add(separator, 0, 2)


        grid.padding = Insets.EMPTY
        VBox.setMargin(grid, Insets.EMPTY)
        children.add(grid)
        this.layout()
    }

    fun initialize() {

        val m = model!!
        nameField.textProperty().bindBidirectional(m.nameProperty())
        hostField.textProperty().bindBidirectional(m.hostProperty())
        val main = FluentViewLoader.javaView(PropertiesView::class.java).providedScopes(m.propertiesScope).load()

        children.add(main.view)
    }
}
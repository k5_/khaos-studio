package eu.khaos.studio.fx.environment

import de.saxsys.mvvmfx.Scope
import de.saxsys.mvvmfx.utils.mapping.ModelWrapper
import eu.khaos.studio.environment.CTEnvironment

class EnvironmentScope : Scope {



    var environment = ModelWrapper<CTEnvironment>(CTEnvironment())

}
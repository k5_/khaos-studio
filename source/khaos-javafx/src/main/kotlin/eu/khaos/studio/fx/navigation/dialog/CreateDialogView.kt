package eu.khaos.studio.fx.navigation.dialog

import javafx.event.EventHandler
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import javafx.stage.Stage


class CreateDialogView(val model: CreateDialogModel) {

    private val main = HBox()

    private val name = TextField()

    private val createRequest = Button("Request")
    private val createFolder = Button("Folder")
    private val createTestcase = Button("Testcase")
    private val stage = Stage()

    init {

        createFolder.onAction = EventHandler { _ -> model.folderClicked.set(!model.folderClicked.get()) }

        createRequest.onAction = EventHandler { _ -> model.requestClicked.set(!model.requestClicked.get()) }

        main.children.addAll(name, createRequest, createFolder)

        model.name.bind(name.textProperty())
    }

    fun asNode(): Parent {
        return main
    }

    fun display() {
        stage.title = "My New Stage Title"
        stage.scene = Scene(asNode(), 450.0, 450.0)
        stage.show()
    }

    fun close() {
        stage.close()
    }
}
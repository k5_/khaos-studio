package eu.khaos.studio.fx.navigation

import javafx.scene.Node
import javafx.scene.control.TitledPane
import javafx.scene.layout.VBox


class NavigationView(
        private val model: NavigationModel
) {

    private val items = ItemsNavView(model)
    private val vbox = VBox()

    init {
        val projectPane = TitledPane()
        projectPane.text = "Items"
        projectPane.content = items.asNode()

        val environmentPane = TitledPane()
        environmentPane.text = "Environments"


        vbox.children.addAll(projectPane, environmentPane)
    }


    fun asNode(): Node {
        return vbox
    }

}
package eu.khaos.server.environment

import eu.khaos.studio.base.CTRequest
import eu.khaos.studio.base.CTResponse

interface Operation<in REQ : CTRequest, out RES : CTResponse> {

    fun invoke(request: REQ): RES

}
package eu.khaos.studio.client

import eu.khaos.storage.*
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.Entity
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.MediaType

class RepositoryClient(private val endpoint: String) : RepositoryResource, Client {
    override fun list(): ProjectTree {
        return target().path("/list").request().get(ProjectTree::class.java)
    }

    private fun target(): WebTarget {
        return ClientBuilder.newClient().target(endpoint)
    }

    override fun project(projectId: String): ProjectResource {
        return ProjectClient(endpoint + "/storage/" + projectId)
    }

    override fun createProject(createProject: CreateProject): Project {
        val response = target().path("storage").request().put(Entity.entity(createProject, MediaType.APPLICATION_JSON))

        return handle(response, Project::class.java)
    }


}
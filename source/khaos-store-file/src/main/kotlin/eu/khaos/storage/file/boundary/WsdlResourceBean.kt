package eu.khaos.storage.file.boundary

import eu.khaos.storage.Project
import eu.khaos.storage.wsdl.WsdlResource

class WsdlResourceBean(
        val repository: Repository,
        val project: Project
) : WsdlResource {

    override fun updateWsdl() {

    }

}
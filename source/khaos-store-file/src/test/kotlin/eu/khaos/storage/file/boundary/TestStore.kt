package eu.khaos.storage.file.boundary

import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

class TestStore {

    companion object {

        fun tempRepository():RepositoryResourceBean {
            return RepositoryResourceBean(tempPath())
        }

        private fun tempPath(): Path {
            val random = Random()
            return Paths.get("target", "repository", "_" + System.currentTimeMillis() + "_" + random.nextInt(1000))
        }

    }
}
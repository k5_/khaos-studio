package eu.khaos.studio.runner.boundary

import eu.khaos.runner.CreateRun
import eu.khaos.runner.RunRef
import eu.khaos.runner.RunResource
import eu.khaos.runner.RunnerResource
import eu.khaos.studio.runner.control.WsdlRequestRunner
import eu.khaos.studio.runner.model.HttpRequest

class RunnerResourceBean : RunnerResource {

    override fun createRunner(create: CreateRun): RunRef {

        val run = HttpRequest(
                body = create.requestBody!!,
                mimeType = create.mimeType!!,
                uri = create.uri!!
        )

        WsdlRequestRunner(run).go()



        return RunRef()
    }

    override fun run(): RunResource {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}
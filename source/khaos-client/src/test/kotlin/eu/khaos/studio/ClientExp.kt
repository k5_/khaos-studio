package eu.khaos.studio

import eu.khaos.storage.AddItemRequest
import eu.khaos.storage.AddWsdlRequest
import eu.khaos.storage.CreateProject
import eu.khaos.studio.client.KhaosClient
import org.apache.commons.lang3.RandomStringUtils

fun main(args: Array<String>) {

    val repository = KhaosClient("http://localhost:8080/api").storage()

    val projectList = repository.list()
    projectList.items?.forEach {
        println("" + it.type + " " + it.path + ": " + it.name)
    }

    val createProject = CreateProject()
    createProject.name = RandomStringUtils.randomAlphabetic(5).toLowerCase()
    val result = repository.createProject(createProject)
    println(result.self!!)
    val project = repository.project(result.projectId!!)


    var createRequest = AddItemRequest()
    createRequest.name = "test"
    val req = project.createItem(createRequest)

    createRequest.name = "abc"
    project.createItem(createRequest)

    val request = project.request(req.name!!)
    request.content("test");
    println(request.content())

    val projectTree = project.list()

    projectTree.items?.forEach { println(it.name + " " + it.parent!!.path) }


}
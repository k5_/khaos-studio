package eu.khaos.studio.fx

import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.cdi.MvvmfxCdiApplication
import eu.k5.arch.menu.MenuView
import javafx.application.Application
import javafx.scene.Scene
import javafx.stage.Stage

fun main(args: Array<String>) {
    Application.launch(TestApp::class.java)
}

class TestApp : MvvmfxCdiApplication() {
    override fun startMvvmfx(stage: Stage) {


        val main = FluentViewLoader.javaView(TestView::class.java).load()
        val rootScene = Scene(main.getView())

        stage.scene = rootScene
        stage.show();
    }

}
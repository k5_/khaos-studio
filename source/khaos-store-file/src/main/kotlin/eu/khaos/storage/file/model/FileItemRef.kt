package eu.khaos.storage.file.model

import java.nio.file.Path

class FileItemRef (
        val project: FileProjectRef,
        val id: String,
        val path: Path
)
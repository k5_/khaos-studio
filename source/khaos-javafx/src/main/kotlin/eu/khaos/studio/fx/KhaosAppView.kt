package eu.khaos.studio.fx

import eu.khaos.studio.fx.items.ItemsControl
import eu.khaos.studio.fx.navigation.ProjectItem
import eu.khaos.studio.fx.navigation.NavigationControl
import eu.khaos.studio.fx.wsdlrequest.WsdlRequestControl
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox

class KhaosAppView(model: KhaosAppModel) {

    private val projects = NavigationControl(model)

    private val items = ItemsControl(model)


    private fun asNode(): Parent {
        val hBox = HBox()
        hBox.children.add(projects.asNode())
        hBox.children.add(items.asNode())
        return hBox
    }


    fun asScene(): Scene {
        val scene = Scene(asNode(), 500.0, 400.0)
        return scene
    }

}
package eu.khaos.studio.client

import eu.khaos.runner.CreateRun
import eu.khaos.runner.RunRef
import eu.khaos.runner.RunResource
import eu.khaos.runner.RunnerResource
import javax.ws.rs.client.Entity
import javax.ws.rs.core.MediaType

class RunnerClient(val endpoint: String) : RunnerResource, Client {

    override fun createRunner(create: CreateRun): RunRef {
        val response = target(endpoint).path("/runs").request().put(Entity.entity(create, MediaType.APPLICATION_JSON_TYPE))
        return handle(response, RunRef::class.java)
    }

    override fun run(): RunResource {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

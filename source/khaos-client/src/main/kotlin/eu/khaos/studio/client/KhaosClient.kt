package eu.khaos.studio.client

import eu.khaos.runner.RunnerResource
import eu.khaos.storage.ProjectResource
import eu.khaos.storage.RepositoryResource
import eu.khaos.storage.request.RequestResource
import sun.misc.Request
import javax.ws.rs.client.ClientBuilder


class KhaosClient(private val endpoint: String) {


    fun request(path: String): RequestResource {
        return RequestClient(endpoint + "/storage" + path)
    }

    fun storage(): RepositoryResource {
        return RepositoryClient(endpoint + "/storage")
    }

    fun runner(): RunnerResource {
        return RunnerClient(endpoint + "/runner")
    }

    fun project(path: String): ProjectResource {
        return ProjectClient(endpoint + "/storage" + path)
    }
}
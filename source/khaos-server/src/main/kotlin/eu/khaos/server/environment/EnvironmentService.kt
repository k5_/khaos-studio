package eu.khaos.server.environment

import eu.khaos.studio.environment.*
import eu.khaos.studio.service.Environment
import javax.inject.Inject

class EnvironmentService @Inject constructor(
        private val getEnvironments: GetEnvironmentsOperation,
        private val updateEnvironments: UpdateEnvironmentOperation
)


    : Environment {


    override fun createEnvironment(body: CreateEnvironmentRequest?): CreateEnvironmentResponse {

        return OBJECT_FACTORY.createCreateEnvironmentResponse()
    }

    override fun getEnvironments(body: GetEnvironmentsRequest): GetEnvironmentsResponse {
        val response = OBJECT_FACTORY.createGetEnvironmentsResponse()
        response.response = getEnvironments.invoke(body.request)
        return response
    }


    override fun updateEnvironment(body: UpdateEnvironmentRequest): UpdateEnvironmentResponse {
        val response = OBJECT_FACTORY.createUpdateEnvironmentResponse()
        response.response = updateEnvironments.invoke(body.request)
        return response
    }


    companion object {
        private val OBJECT_FACTORY = ObjectFactory()
    }

}
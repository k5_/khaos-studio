package eu.khaos.storage.file.model

import java.nio.file.Path

class FileRequestRef(
        val project: FileProjectRef,
        val requestId: String,
        val path: Path
)
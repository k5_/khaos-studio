package eu.khaos.storage

import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Path("storage")
interface RepositoryResource {

    @PUT
    @Path("/storage")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun createProject(createProject: CreateProject): Project

    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    fun list(): ProjectTree

    @Path("/storage/{projectId}")
    fun project(@PathParam("projectId") projectId: String): ProjectResource


}
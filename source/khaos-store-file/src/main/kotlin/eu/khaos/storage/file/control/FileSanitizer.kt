package eu.khaos.storage.file.control

import eu.khaos.storage.ItemRef
import java.util.regex.Pattern

class FileSanitizer {

    private val sanitizePattern = Pattern.compile("[^0-9A-Za-z_]")

    fun sanitize(name: String): String {
        return sanitizePattern.matcher(name).replaceAll("_")
    }

    fun createId(name: String, parent: ItemRef?): String {
        val localPath = sanitize(name)

        return if (parent == null) {
            localPath
        } else {
            parent.id + "!" + localPath
        }
    }


}
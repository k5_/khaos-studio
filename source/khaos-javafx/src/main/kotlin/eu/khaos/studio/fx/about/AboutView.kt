package eu.k5.arch.about

import de.saxsys.mvvmfx.JavaView
import javafx.scene.control.Label
import javafx.scene.layout.VBox
import javafx.stage.Stage
import javax.inject.Inject

class AboutView : VBox(), JavaView<AboutViewModel> {

    var label = Label("About")

    init {
        children.addAll(label)
    }

}
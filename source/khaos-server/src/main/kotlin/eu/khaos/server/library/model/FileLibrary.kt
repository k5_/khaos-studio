package eu.khaos.server.library.model

import javax.xml.bind.annotation.*

@XmlRootElement(name = "library")
@XmlAccessorType(XmlAccessType.NONE)
class FileLibrary {

    @XmlElement
    var name: String? = null

    @XmlElement
    var `interface`: List<FileInterface> = ArrayList()


}

@XmlAccessorType(XmlAccessType.NONE)
class FileInterface {

    @XmlElement
    var name: String? = null

    @XmlElement
    var path: String? = null

    @XmlElement
    var descriptor: String? = null

    @XmlElement
    var operation: List<FileOperation> = ArrayList()

}

@XmlAccessorType(XmlAccessType.NONE)
class FileOperation {

    @XmlElement
    var name: String? = null

    @XmlElement
    var readOnly: Boolean? = null
}
package eu.khaos.studio.fx.nav

import de.saxsys.mvvmfx.ViewModel
import eu.khaos.storage.ItemRef
import eu.khaos.storage.ItemType
import eu.khaos.studio.client.KhaosClient
import eu.khaos.studio.fx.OpenItemEvent
import eu.khaos.studio.fx.navigation.ProjectItem
import eu.khaos.studio.fx.navigation.ProjectItemType
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.ObservableList
import javax.enterprise.event.Event
import javax.inject.Inject

class NavItemsViewModel @Inject constructor(

        private val openRequest: Event<OpenItemEvent>

) : ViewModel {

    val root = ProjectItem("")

    val items: ObservableList<ProjectItem> = root.children

    val selected = SimpleObjectProperty<ProjectItem?>()
    val client = KhaosClient("http://localhost:8080/api")

    fun initialize() {

        val storage = client.storage()
        val tree = storage.list()

        addItemList(root, tree?.items.orEmpty())
        selected.addListener { _, _, newValue -> selectionChanged(newValue) }
    }

    fun openItem() {
        println("OpenItem")
        val current = selected.get()
        if (current != null) {
            openRequest.fire(OpenItemEvent(current.name.get(), current.project.get(), current.path.get(), current.type.get()))
        }
    }

    fun selectionChanged(projectItem: ProjectItem?) {
        if (projectItem == null) {
            return
        }
        println("select")
        if (projectItem.type.get() == ProjectItemType.REQUEST) {
            //  cmodel.currentItem.set(projectItem)
        } else if (projectItem.type.get() == ProjectItemType.PROJECT) {
            val project = client.project(projectItem.path.get()).list()

            addItemList(projectItem, project.items!!)

        }
    }


    private fun addItemList(addRoot: ProjectItem, items: List<ItemRef>) {
        for (newItem in items.orEmpty()) {
            println(newItem.path)
            if (newItem.id!!.contains("!")) {
                val pathParts = newItem.id!!.split("!")

                var root: ProjectItem? = addRoot
                for ((index, part) in pathParts.withIndex()) {
                    val child = root!!.getChild(part)
                    if (child != null) {
                        root = child
                    } else if (index == pathParts.size - 1) {
                        root = addItem(root, newItem, part)
                    } else {
                        root = addFolder(root, part)
                    }
                }
            } else {
                addItem(addRoot, newItem, newItem.id!!)
            }
        }
    }

    private fun addFolder(projectItem: ProjectItem, part: String): ProjectItem {
        val item = ProjectItem(part)
        item.type.set(ProjectItemType.FOLDER)
        item.name.set(part)
        projectItem.children.add(item)
        return item

    }

    private fun contains(projectItem: ProjectItem, path: String): Boolean {
        for (child in projectItem.children) {
            if (child.localPath == path) {
                return true
            }
        }
        return false
    }

    private fun addItem(projectItem: ProjectItem, newItem: ItemRef, localPath: String): ProjectItem? {
        if (!contains(projectItem, localPath)) {
            val item = ProjectItem(localPath)
            item.id.set(newItem.id)
            item.name.set(newItem.name)
            item.path.set(newItem.path)
            item.project.set(projectItem.project.get())
            if (newItem.type == ItemType.REQUEST) {
                item.type.set(ProjectItemType.REQUEST)
            } else if (newItem.type == ItemType.PROJECT) {
                item.type.set(ProjectItemType.PROJECT)
                item.project.set(newItem.path)
            } else if (newItem.type == ItemType.FOLDER) {
                item.type.set(ProjectItemType.FOLDER)
            }
            projectItem.children.add(item)
            return item
        } else {
            return projectItem.getChild(localPath)
        }
    }
}
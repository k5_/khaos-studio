package eu.khaos.storage.file.boundary

import eu.khaos.storage.ItemRef
import eu.khaos.storage.ItemType
import eu.khaos.storage.ProjectTree
import eu.khaos.storage.file.control.FileModel
import eu.khaos.storage.file.model.FileFolder
import eu.khaos.storage.file.model.FileProject
import eu.khaos.storage.file.model.FileRequest
import eu.khaos.studio.storage.CTGetProjectsRequest
import eu.khaos.studio.storage.CTGetProjectsResponse
import java.io.IOException
import java.nio.file.FileVisitResult
import java.nio.file.FileVisitor
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes
import java.util.*
import java.util.regex.Pattern
import javax.ws.rs.core.Response

class StorageRepository(
        private val path: Path
) {


    fun getWsdlRequest(project: String, path: String): FileRequest {
        val projectPath = resolvePojectPath(project)
        val resolvePath = resolvePath(projectPath, path)

        return FileModel().fromXml(resolvePath, FileRequest::class.java)
    }

    private fun resolvePojectPath(uri: String): Path {
        if (!StorageRepository.uriPattern.matcher(uri).matches()) {
            println(uri)
            throw ConsumerException("Invalid URI", Response.Status.BAD_REQUEST);
        }
        return path.resolve(uri)
    }

    private fun resolvePath(projectPath: Path, itemId: String): Path {
        if (!itemId.contains('!')) {
            return projectPath.resolve(itemId)
        } else {
            var folder = projectPath
            for (part in itemId.split("!")) {
                folder = folder.resolve(part)
            }
            return folder
        }
    }

    fun listProjects(): List<ItemRef> {
        val tree = ProjectTree()
        Files.walkFileTree(path, ProjectTreeVisitor(tree, model))

        return tree.items!!
    }

    companion object {
        private val uriPattern = Pattern.compile("[a-z]{3,10}")
        private val model = FileModel()
    }

}


private class ProjectTreeVisitor(val tree: ProjectTree, val model: FileModel) : FileVisitor<Path> {
    private val path = ArrayDeque<String>()

    override fun postVisitDirectory(dir: Path?, exc: IOException?): FileVisitResult {
        path.pop()
        return FileVisitResult.CONTINUE
    }

    override fun visitFileFailed(file: Path?, exc: IOException?): FileVisitResult {
        return FileVisitResult.CONTINUE
    }

    override fun preVisitDirectory(dir: Path, attrs: BasicFileAttributes?): FileVisitResult {

        if (Files.exists(dir.parent.resolve("project.xml"))) {
            return FileVisitResult.SKIP_SUBTREE
        }
        path.push(dir.fileName.toString())

        if (!Files.exists(dir.resolve("project.xml"))
                && !Files.exists(dir.resolve("request.xml"))) {

            val folderFile = dir.resolve("folder.xml")

            val ref = ItemRef()
            ref.path = "/folder/" + path.reversed().subList(1, path.size).joinToString("!")
            ref.id = path.reversed().subList(1, path.size).joinToString("!")
            ref.type = ItemType.FOLDER
            if (Files.exists(folderFile)) {

                val fileFolder = model.fromXml(folderFile, FileFolder::class.java)
                ref.name = fileFolder.name

            } else {
                ref.name = dir.fileName.toString()
            }
            tree.items!!.add(ref)
        }
        return FileVisitResult.CONTINUE
    }

    override fun visitFile(file: Path?, attrs: BasicFileAttributes?): FileVisitResult {
        if ("project.xml" == file!!.fileName.toString()) {
            val fileProject = model.fromXml(file!!, FileProject::class.java)
            val ref = ItemRef()
            ref.path = "/storage/" + path.reversed().subList(1, path.size).joinToString("!")
            ref.id = path.reversed().subList(1, path.size).joinToString("!")
            ref.name = fileProject.name
            ref.type = ItemType.PROJECT
            tree.items!!.add(ref)
            return FileVisitResult.SKIP_SIBLINGS
        }
        return FileVisitResult.CONTINUE
    }
}
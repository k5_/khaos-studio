package eu.k5.arch.request

import de.saxsys.mvvmfx.InjectScope
import de.saxsys.mvvmfx.ScopeProvider
import de.saxsys.mvvmfx.ViewModel
import javax.inject.Provider


class RequestViewModel(

) : ViewModel {


    @InjectScope
    var requestScope: RequestScope? = null

}
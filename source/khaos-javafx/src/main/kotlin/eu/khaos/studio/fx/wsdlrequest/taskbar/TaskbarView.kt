package eu.khaos.studio.fx.wsdlrequest.taskbar

import de.saxsys.mvvmfx.JavaView
import eu.khaos.studio.fx.toggle
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.layout.HBox

class TaskbarView : HBox(), JavaView<TaskbarModel> {

    private val start: Button
    private val stop: Button
    private val save: Button

    private val url: TextField

    init {
        start = Button("start")
        stop = Button("stop")
        save = Button("save")
        url = TextField()
        //url.textProperty().bind(model.requestModel.url)
        children.addAll(start, stop, url, save)
    }

    fun initialize() {
//        start.onAction = EventHandler { _ -> model.requestModel.start.toggle() }
        //       save.onAction = EventHandler { _ -> model.requestModel.save.toggle() }
        //      model.requestModel.url.bind(url.textProperty())

    }


}
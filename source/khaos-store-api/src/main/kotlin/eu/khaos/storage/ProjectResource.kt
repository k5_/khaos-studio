package eu.khaos.storage

import eu.khaos.storage.request.RequestRef
import eu.khaos.storage.request.RequestResource
import eu.khaos.storage.wsdl.AddWsdl
import javax.print.attribute.standard.Media
import javax.ws.rs.*
import javax.ws.rs.core.MediaType


@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
interface ProjectResource {


    @Produces("application/json")
    @GET
    fun get(): Project

    @Path("/list")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun list(): ProjectTree


    @PUT
    @Path("/wsdls")
    fun addWsdl(wsdl: AddWsdl)

    @PUT
    @Path("/items")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun createItem(request: AddItemRequest): ItemRef


    @Path("/request/{requestId}")
    fun request(@PathParam("requestId") requestId: String): RequestResource

    @Path("/testcase/{testcaseId}")
    fun testcase(@PathParam("testcaseId") testcaseId: String): TestcaseResource

}
package eu.khaos.storage.util

class Properties {

    var property: MutableList<Property>? = null

}

class Property (
    var key: String? = null,
    var value: String? = null
){}
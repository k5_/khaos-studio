package eu.khaos.studio.fx.wsdlrequest

import de.saxsys.mvvmfx.Initialize
import de.saxsys.mvvmfx.InjectScope
import de.saxsys.mvvmfx.ViewModel
import eu.khaos.studio.fx.caller.Caller
import eu.khaos.studio.project.GetWsdlRequestRequest
import eu.khaos.studio.project.GetWsdlRequestResponse
import eu.khaos.studio.project.ObjectFactory
import eu.khaos.studio.service.Project
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javax.inject.Inject

class WsdlRequestModel @Inject constructor(
        private val project: Caller<Project>
) : ViewModel {

    @InjectScope
    var scope: WsdlRequestScope? = null

    val start = SimpleBooleanProperty(false)

    val save = SimpleBooleanProperty(false)

    val url = SimpleStringProperty()

    val content = SimpleStringProperty()

    @Initialize
    fun ini() {
        project.success(this::loaded).call().getWsdlRequest(createGetWsdlRequest(scope!!.project, scope!!.path))
    }

    private fun loaded(response: GetWsdlRequestResponse?) {
        val ctWsdlRequest = response!!.response.request
        scope!!.request.set(ctWsdlRequest)
    }

    companion object {
        val OBJECT_FACTORY = ObjectFactory()
        fun createGetWsdlRequest(project: String, key: String): GetWsdlRequestRequest? {
            val request = OBJECT_FACTORY.createGetWsdlRequestRequest()
            request.request = OBJECT_FACTORY.createCTGetWsdlRequestRequest()
            request.request.ref = OBJECT_FACTORY.createCTProjectItemRef()
            request.request.ref.project = project
            request.request.ref.key = key
            return request
        }
    }
}
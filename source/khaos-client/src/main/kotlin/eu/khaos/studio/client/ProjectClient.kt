package eu.khaos.studio.client

import eu.khaos.storage.*
import eu.khaos.storage.request.RequestRef
import eu.khaos.storage.request.RequestResource
import eu.khaos.storage.wsdl.AddWsdl
import javax.ws.rs.client.Entity
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.MediaType

class ProjectClient(private val endpoint: String) : ProjectResource, Client {
    override fun testcase(testcaseId: String): TestcaseResource {
        return TestcaseClient("$endpoint/testcase$testcaseId")
    }


    override fun list(): ProjectTree {
        return target(endpoint).path("/list").request().get(ProjectTree::class.java)
    }

    override fun target(endpoint: String): WebTarget {
        return super.target(endpoint)
    }

    override fun get(): Project {
        return target(endpoint).request().get(Project::class.java)
    }

    override fun addWsdl(wsdl: AddWsdl) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createItem(request: AddItemRequest): ItemRef {
        val response = target(endpoint).path("items")
                .request()
                .put(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE))

        return handle(response, ItemRef::class.java)
    }

    override fun request(requestId: String): RequestResource {
        return RequestClient("$endpoint/request/$requestId")
    }
}
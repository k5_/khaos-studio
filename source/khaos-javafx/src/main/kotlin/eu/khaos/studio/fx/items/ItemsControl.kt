package eu.khaos.studio.fx.items

import eu.khaos.studio.fx.KhaosAppModel
import eu.khaos.studio.fx.MessageBus
import eu.khaos.studio.fx.OpenItemEvent
import javafx.scene.Node

class ItemsControl(val cmodel: KhaosAppModel) {

    private val model = ItemsModel()
    private val view = ItemsView(model)

    private val observer: (OpenItemEvent) -> Unit = { openItem(it) }

    init {
        MessageBus.observe(OpenItemEvent::class.java, observer)
    }

    private fun openItem(event: OpenItemEvent) {
        println("open " + event.path)
       // view.openCurrent(event.name, event.path)
    }


    fun asNode(): Node {

        return view.asNode()
    }
}
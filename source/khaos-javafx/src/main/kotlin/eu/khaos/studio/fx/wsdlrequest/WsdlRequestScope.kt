package eu.khaos.studio.fx.wsdlrequest

import de.saxsys.mvvmfx.Scope
import eu.khaos.studio.project.CTWsdlRequest
import javafx.beans.property.SimpleObjectProperty

class WsdlRequestScope(
        val project: String,
        val path: String
) : Scope {

    val request = SimpleObjectProperty<CTWsdlRequest>()

}
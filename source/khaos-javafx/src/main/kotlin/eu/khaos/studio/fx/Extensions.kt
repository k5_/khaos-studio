package eu.khaos.studio.fx

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty

fun SimpleBooleanProperty.toggle(): Unit {
    set(!get())
}
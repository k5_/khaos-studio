package eu.khaos.studio.fx.caller

import eu.khaos.studio.service.Environment
import eu.khaos.studio.service.EnvironmentService
import java.lang.IllegalArgumentException
import java.lang.reflect.ParameterizedType
import javax.enterprise.inject.Produces
import javax.enterprise.inject.spi.InjectionPoint
import javax.xml.ws.BindingProvider


class CallerProducer() {



    private fun <T> createInstance(type: Class<T>): T {
        if (type == Environment::class.java) {
            val env = EnvironmentService()
            val port = env.environmentPort
            val bp = port as BindingProvider
            bp.requestContext[BindingProvider.ENDPOINT_ADDRESS_PROPERTY] = "http://127.0.0.1:8080/soap/environment"
            return type.cast(port) as T
        }

        TODO("implement")
    }

    @Produces
    fun <T> create(ip: InjectionPoint): Caller<T> {
        val type = ip.type
        if (type !is ParameterizedType) {
            throw IllegalArgumentException("only parameterized types supported")
        }
        val service = type.actualTypeArguments[0] as Class<T>
        return CallerImpl(service, createInstance(service))
    }
}
package eu.khaos.studio.fx.utils.properties

import de.saxsys.mvvmfx.Initialize
import de.saxsys.mvvmfx.InjectScope
import de.saxsys.mvvmfx.ViewModel
import eu.khaos.studio.base.CTProperties
import eu.khaos.studio.base.CTProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.event.ActionEvent
import javafx.event.EventHandler

class PropertiesViewModel : ViewModel {

    @InjectScope
    private var scope: PropertiesScope? = null

    val properties = FXCollections.observableArrayList<FxProperty>()!!

    val selected = SimpleObjectProperty<FxProperty?>()

    val canDelete = SimpleBooleanProperty()

    val addAction: EventHandler<ActionEvent> = EventHandler { add() }

    val deleteAction: EventHandler<ActionEvent> = EventHandler { delete() }

    @Initialize
    fun ini() {
        scope!!.properties.get().property.mapTo(properties) { asFxProperty(it) }

        selected.addListener { _, _, newValue -> selectionChanged(newValue) }
    }

    private fun selectionChanged(newValue: FxProperty?) {
        if (newValue == null) {
            canDelete.set(false)
        } else {
            canDelete.set(true)
        }
    }

    private fun delete() {
        val currentSelected = selected.get()
        if (currentSelected != null) {
            properties.remove(currentSelected)
        }
    }

    private fun add() {
        val fxProperty = FxProperty()
        properties.add(fxProperty)
    }

    private fun asFxProperty(ctProperty: CTProperty): FxProperty {
        val fxProperty = FxProperty()
        fxProperty.name.set(ctProperty.key)
        fxProperty.value.set(ctProperty.value)
        return fxProperty
    }

    class FxProperty {
        val name = SimpleStringProperty()
        val value = SimpleStringProperty()
    }
}
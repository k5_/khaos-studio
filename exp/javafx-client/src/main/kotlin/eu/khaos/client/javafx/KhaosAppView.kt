package eu.khaos.client.javafx

import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox

class KhaosAppView(model: KhaosAppModel) {

    private val projects = ProjectTreeView(model)

    private fun asNode(): Parent {

        val hbox = HBox()

        hbox.children.add(projects.asNode())
        hbox.children.add(VBox())
        return hbox
    }

    fun asScene(): Scene {
        val scene = Scene(asNode(), 500.0, 400.0)
        return scene
    }

}
package eu.k5.arch.request

import de.saxsys.mvvmfx.Scope
import de.saxsys.mvvmfx.ScopeProvider
import de.saxsys.mvvmfx.ViewModel
import java.util.*
import java.util.function.BiConsumer
import javax.inject.Provider

@ScopeProvider(scopes = [RequestScope::class])
class MainViewModel(val requestScopeProvider: Provider<RequestScope>) : ViewModel {

    private var openTabConsumer: BiConsumer<String, List<Scope>>? = null

    fun openRequest(id: String) {

        val request = Request()
        request.title = "title"
        request.content = "abc"

        val scope = requestScopeProvider.get()
        scope.requestProperty().set(request)

        openTabConsumer!!.accept(request.title!!, Arrays.asList(scope))

    }

    fun onOpenDocument(openTabConsumer: BiConsumer<String, List<Scope>>) {
        this.openTabConsumer = openTabConsumer
    }
}
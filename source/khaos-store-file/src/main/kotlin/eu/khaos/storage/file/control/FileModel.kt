package eu.khaos.storage.file.control

import eu.khaos.storage.file.model.FileFolder
import eu.khaos.storage.file.model.FileProject
import eu.khaos.storage.file.model.FileRequest
import java.io.StringReader
import java.io.StringWriter
import java.nio.file.Files
import java.nio.file.Path
import javax.xml.bind.JAXBContext

class FileModel {
    fun toPath(obj: Any, path: Path) {
        write(path, obj)
    }

    fun <T> fromXml(path: Path, type: Class<T>): T {
        val objc = context.createUnmarshaller().unmarshal(path.toFile())
        if (type.isInstance(objc)) {
            return type.cast(objc)
        }
        throw IllegalArgumentException("Unable to parse xml to type")
    }

    fun <T> fromXml(xml: String, type: Class<T>): T {
        val objc = context.createUnmarshaller().unmarshal(StringReader(xml))
        if (type.isInstance(objc)) {
            return type.cast(objc)
        }
        throw IllegalArgumentException("Unable to parse xml to type")
    }

    fun write(path: Path, obj: Any) {
        Files.createDirectories(path.parent)

        val marshaller = context.createMarshaller()
        marshaller.marshal(obj, path.toFile())

    }

    companion object {
        val context = JAXBContext.newInstance(FileProject::class.java, FileRequest::class.java, FileFolder::class.java)

        fun toXml(obj: Any): String {
            val marshaller = context.createMarshaller()
            var stringWriter = StringWriter()
            marshaller.marshal(obj, stringWriter)
            return stringWriter.toString()
        }


    }
}
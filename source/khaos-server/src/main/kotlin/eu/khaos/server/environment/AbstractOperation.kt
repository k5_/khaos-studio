package eu.khaos.server.environment

import eu.khaos.studio.base.CTRequest
import eu.khaos.studio.base.CTResponse

abstract class AbstractOperation<in REQ : CTRequest, out RES : CTResponse>(
        private val responseSupplier: () -> RES
) :  Operation<REQ, RES> {

    fun success(): RES {
        return responseSupplier.invoke()
    }

}
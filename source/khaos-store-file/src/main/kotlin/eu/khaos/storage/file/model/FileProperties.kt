package eu.khaos.storage.file.model

import eu.khaos.storage.util.Properties
import eu.khaos.storage.util.Property
import javax.xml.bind.annotation.*

@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
class FileProperties {

    @XmlElement(name = "property")
    var property: MutableList<FileProperty>? = null

    @XmlAccessorType(XmlAccessType.NONE)
    class FileProperty {

        @XmlAttribute(name = "key")
        var key: String? = null

        @XmlValue
        var value: String? = null
    }

    fun asProperties(): Properties {
        val properties = Properties()
        properties.property = ArrayList()
        for (fileProperty in this.property.orEmpty()) {
            properties.property!!.add(Property(fileProperty.key, fileProperty.value))
        }
        return properties;
    }

    companion object {

        fun fromProperties(properties: Properties): FileProperties {
            val fileProperties = FileProperties()
            fileProperties.property = ArrayList()
            for (property in properties.property.orEmpty()) {
                val fileProperty = FileProperty()
                fileProperty.key = property.key
                fileProperty.value = property.value
                fileProperties.property!!.add(fileProperty)
            }
            return fileProperties;
        }

    }
}


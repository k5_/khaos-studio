package eu.k5.arch.request

import de.saxsys.mvvmfx.Scope
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty


class RequestScope : Scope {
    private val request = SimpleObjectProperty<Request>()


    fun requestProperty(): ObjectProperty<Request> = request


}
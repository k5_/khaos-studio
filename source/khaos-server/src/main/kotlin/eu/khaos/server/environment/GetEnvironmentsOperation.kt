package eu.khaos.server.environment

import eu.khaos.studio.environment.CTEnvironment
import eu.khaos.studio.environment.CTGetEnvironmentsRequest
import eu.khaos.studio.environment.CTGetEnvironmentsResponse
import eu.khaos.studio.environment.ObjectFactory
import java.util.*
import javax.inject.Inject


class GetEnvironmentsOperation @Inject constructor(
        private val repository: EnvironmentRepository
) : Operation<CTGetEnvironmentsRequest, CTGetEnvironmentsResponse> {


    override fun invoke(request: CTGetEnvironmentsRequest): CTGetEnvironmentsResponse {
        Thread.sleep(1000)
        val ctResponse = ObjectFactory().createCTGetEnvironmentsResponse()
        ctResponse.environment.addAll(repository.all())
        return ctResponse
    }

}
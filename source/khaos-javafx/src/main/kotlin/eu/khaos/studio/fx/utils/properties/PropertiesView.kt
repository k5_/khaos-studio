package eu.khaos.studio.fx.utils.properties

import de.saxsys.mvvmfx.InjectViewModel
import de.saxsys.mvvmfx.JavaView
import eu.khaos.studio.fx.Icons
import javafx.scene.control.Button
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.util.Callback

class PropertiesView : VBox(), JavaView<PropertiesViewModel> {

    private val table = TableView<PropertiesViewModel.FxProperty>()

    @InjectViewModel
    private var model: PropertiesViewModel? = null

    private val add: Button
    private val delete: Button

    init {

        val tools = HBox()

        add = Button("", Icons.ADD.get())
        delete = Button("delete")
        tools.children.addAll(add, delete)

        val nameColumn = TableColumn<PropertiesViewModel.FxProperty, String>()
        nameColumn.prefWidth = 75.0
        nameColumn.cellValueFactory = Callback { it.value.name }

        val valueColumn = TableColumn<PropertiesViewModel.FxProperty, String>()
        valueColumn.prefWidth = 75.0
        valueColumn.cellValueFactory = Callback { it.value.value }

        table.columns.addAll(nameColumn, valueColumn)

        children.addAll(tools, table)
    }

    fun initialize() {
        table.items = model!!.properties

        model!!.selected.bind(table.selectionModel.selectedItemProperty())

        add.onAction = model!!.addAction
        delete.onAction = model!!.deleteAction
        delete.disableProperty().bind(model!!.canDelete.not())
    }
}
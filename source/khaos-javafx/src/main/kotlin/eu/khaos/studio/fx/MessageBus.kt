package eu.khaos.studio.fx

import java.lang.ref.WeakReference
import java.util.*

object MessageBus {

    private val callbacks = HashMap<Class<*>, Callbacks<*>>()

    fun fire(event: Any) {

        val cbs = callbacks.get(event.javaClass) ?: return

        if (event.javaClass == cbs.type) {
            cbs.fire(event)
        }

    }


    fun <T> observe(type: Class<T>, callback: (T) -> Unit) {
        if (callbacks.containsKey(type)) {
            (callbacks.get(type) as Callbacks<T>).add(callback)
        } else {
            val cbs = Callbacks<T>(type)
            cbs.add(callback)
            callbacks.put(type, cbs)
        }
    }


    class Callbacks<T>(val type: Class<T>) {
        private val callbacks = ArrayList<WeakReference<(T) -> Unit>>()

        fun fire(event: Any) {
            if (type.isInstance(event)) {
                fireConcrete(type.cast(event))
            } else {
                throw IllegalArgumentException("Unsupported type")
            }
        }

        private fun fireConcrete(event: T) {
            val remove =  ArrayList<WeakReference<(T) -> Unit>>()
            for (callback in callbacks) {
                val get = callback.get()
                if (get == null) {
                    remove.add(callback)
                } else {
                    get(event)
                }
            }
            callbacks.removeAll(remove)
        }

        fun add(callback: (T) -> Unit) {
            callbacks.add(WeakReference(callback))
        }
    }
}
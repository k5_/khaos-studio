package eu.khaos.studio.fx.nav

import de.saxsys.mvvmfx.ViewModel
import eu.khaos.studio.library.ObjectFactory
import eu.khaos.studio.fx.OpenItemEvent
import eu.khaos.studio.fx.caller.Caller
import eu.khaos.studio.library.GetLibrariesRequest
import eu.khaos.studio.library.GetLibrariesResponse
import eu.khaos.studio.service.Environment
import eu.khaos.studio.service.Library
import javax.enterprise.event.Event
import javax.inject.Inject

class NavLibraryViewModel @Inject constructor(
        private val caller: Caller<Library>,
        private val openRequest: Event<OpenItemEvent>
) : ViewModel {

    init {

        caller.success(this::loadLibraries).call().getLibraries(createGetLibrariesRequest())
    }

    fun loadLibraries(getLibrary: GetLibrariesResponse?) {
        if (getLibrary == null){
            return
        }



    }


    companion object {
        private val OBJECT_FACTORY = ObjectFactory()

        private fun createGetLibrariesRequest(): GetLibrariesRequest {
            val request = OBJECT_FACTORY.createGetLibrariesRequest()
            request.request = OBJECT_FACTORY.createCTGetLibrariesRequest()
            return request
        }
    }
}
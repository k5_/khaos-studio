package eu.khaos.studio.fx.items

import eu.khaos.studio.fx.navigation.ProjectItem
import eu.khaos.studio.fx.wsdlrequest.WsdlRequestControl
import javafx.scene.Node
import javafx.scene.control.Tab
import javafx.scene.control.TabPane

class ItemsView(val model: ItemsModel) {

    private val tabPane = TabPane()

    fun asNode(): Node {
        return tabPane
    }

    fun openRequest(name: String, path: String) {
        val item = WsdlRequestControl(path)

        val tab = Tab()
        tab.text = name
        tab.content = item.asNode()

        tabPane.tabs.add(tab)
    }


}

package eu.k5.arch.request

class Request {
    var title: String? = null
    var content: String? = null
}
package eu.khaos.studio.fx.environment

import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.InjectViewModel
import de.saxsys.mvvmfx.JavaView
import eu.khaos.studio.fx.nav.FxEnvironment
import eu.khaos.studio.fx.utils.Bind
import javafx.scene.control.Button
import javafx.scene.control.SplitPane
import javafx.scene.control.TreeTableColumn
import javafx.scene.control.TreeTableView
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox

class EnvironmentView : VBox(), JavaView<EnvironmentViewModel> {

    //private val splitPane = SplitPane()
    //private val tree = TreeTableView<EnvironmentTreeTableViewModel>()

    private val toolbar = HBox()

    private val save = Button("save")

    @InjectViewModel
    private var model: EnvironmentViewModel? = null

    init {
        //  VBox.setVgrow(this, Priority.ALWAYS)
        // splitPane.maxHeight = Double.MAX_VALUE

        //VBox.setVgrow(splitPane, Priority.ALWAYS)

        //children.add(splitPane)

        toolbar.children.add(save)

        //val column = TreeTableColumn<EnvironmentTreeTableViewModel, String>("Name")


    }


    fun initialize() {
        //splitPane.items.add(0, tree)
        val editView = FluentViewLoader.javaView(EditEnvironmentView::class.java).providedScopes(model!!.scope).load()

        children.add(toolbar)
        children.add(editView.view)
        //splitPane.items.add(1, editView.view)

        //model!!.selected.bind(Bind.treeItem(tree.selectionModel.selectedItemProperty()))

        save.onAction = model!!.saveAction
    }

}
package eu.khaos.server.project.control

import eu.khaos.server.environment.AbstractOperation
import eu.khaos.storage.file.boundary.StorageRepository
import eu.khaos.studio.storage.CTGetWsdlRequestRequest
import eu.khaos.studio.storage.CTGetWsdlRequestResponse
import eu.khaos.studio.storage.ObjectFactory
import javax.inject.Inject

class GetWsdlRequestOperation @Inject constructor(
        private val repository: StorageRepository
) : AbstractOperation<CTGetWsdlRequestRequest, CTGetWsdlRequestResponse>(
        OBJECT_FACTORY::createCTGetWsdlRequestResponse
) {


    override fun invoke(request: CTGetWsdlRequestRequest): CTGetWsdlRequestResponse {

        val ref = request.ref

        val fileRequest = repository.getWsdlRequest(ref.project, ref.key)

        val ctWsdlRequest = OBJECT_FACTORY.createCTWsdlRequest()
        ctWsdlRequest.name = fileRequest.name
        ctWsdlRequest.operation = fileRequest.operation
        //ctWsdlRequest.properties =
        val response = success()
        response.request = ctWsdlRequest
        return response
    }

    companion object {
        val OBJECT_FACTORY = ObjectFactory()
    }
}
package eu.khaos.storage.request

import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path

interface RequestResource {

    @GET
    fun get(): RequestDescription

    @POST
    fun post(update: RequestDescription): RequestDescription

    @GET
    @Path("content")
    fun content(): String

    @POST
    @Path("content")
    fun content(content: String)


}
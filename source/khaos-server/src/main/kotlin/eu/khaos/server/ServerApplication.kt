package eu.khaos.server

import com.google.inject.Guice
import com.google.inject.Module
import com.roskart.dropwizard.jaxws.EndpointBuilder
import com.roskart.dropwizard.jaxws.JAXWSBundle
import eu.khaos.server.environment.EnvironmentRepository
import eu.khaos.server.environment.EnvironmentService
import eu.khaos.server.library.boundary.LibraryService
import eu.khaos.server.library.control.LibraryRepository
import eu.khaos.server.project.boundary.StorageService
import eu.khaos.storage.file.boundary.RepositoryResourceBean
import eu.khaos.storage.file.boundary.StorageRepository
import eu.khaos.studio.runner.boundary.RunnerResourceBean
import io.dropwizard.Application
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import java.nio.file.Paths


fun main(args: Array<String>) {
    ServerApplication().run(*args)
}

class ServerApplication : Application<KhaosConfiguration>() {
    private val jaxWsBundle = JAXWSBundle<KhaosConfiguration>()

    override fun initialize(bootstrap: Bootstrap<KhaosConfiguration>) {
        bootstrap.addBundle(jaxWsBundle)
    }

    override fun run(config: KhaosConfiguration, environment: Environment) {
        val path = Paths.get("D:/test")

        val injector = Guice.createInjector(Module {
            it.bind(EnvironmentRepository::class.java).toInstance(EnvironmentRepository(path))
            it.bind(LibraryRepository::class.java).toInstance(LibraryRepository(path))
            it.bind(StorageRepository::class.java).toInstance(StorageRepository(path))
        })

        val repository = RepositoryResourceBean(Paths.get("D:/test"))

        val runner = RunnerResourceBean()
        environment.jersey().urlPattern = "/api/*"
        environment.jersey().register(repository)

        environment.jersey().register(runner)


        val environmentService = injector.getInstance(EnvironmentService::class.java)
        jaxWsBundle.publishEndpoint(EndpointBuilder("/environment", environmentService))
        val libraryService = injector.getInstance(LibraryService::class.java)
        jaxWsBundle.publishEndpoint(EndpointBuilder("/library", libraryService))
        val storageService = injector.getInstance(StorageService::class.java)
        jaxWsBundle.publishEndpoint(EndpointBuilder("/storage", storageService))



    }
}
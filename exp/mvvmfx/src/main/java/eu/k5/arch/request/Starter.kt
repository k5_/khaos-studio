package eu.k5.arch.request

import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.MvvmFX
import eu.lestard.easydi.EasyDI
import javafx.application.Application
import javafx.scene.Scene
import javafx.stage.Stage

fun main(args: Array<String>) {
    Application.launch(Starter::class.java as Class<out Application>)
}

class Starter : Application() {
    @Throws(Exception::class)
    override fun start(stage: Stage) {
        val easyDI = EasyDI()
        MvvmFX.setCustomDependencyInjector { easyDI.getInstance(it) }

        stage.setTitle("Hello World Application")

        val scope = RequestScope()
        val request = Request()
        request.title = "test"
        request.content = "content"
        scope.requestProperty().set(request)

        val viewTuple = FluentViewLoader.javaView(RequestView::class.java).providedScopes(scope).load()

        val root = viewTuple.view
        stage.scene = Scene(root)
        stage.show()
    }
}
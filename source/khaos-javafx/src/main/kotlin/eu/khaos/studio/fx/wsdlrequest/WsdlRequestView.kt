package eu.khaos.studio.fx.wsdlrequest

import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.InjectViewModel
import de.saxsys.mvvmfx.JavaView
import eu.khaos.studio.fx.nav.NavItemsView
import eu.khaos.studio.fx.wsdlrequest.taskbar.TaskbarControl
import eu.khaos.studio.fx.wsdlrequest.taskbar.TaskbarView
import javafx.geometry.Side
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import javafx.scene.control.TextArea
import javafx.scene.layout.HBox
import javafx.scene.layout.Pane
import javafx.scene.layout.VBox


class WsdlRequestView : TabPane(), JavaView<WsdlRequestModel> {

    @InjectViewModel
    var model: WsdlRequestModel? = null

    private val settings = VBox()

    private val taskbarPane = Pane()

    private val request = TextArea()

    init {

        val settingsTab = Tab()
        settingsTab.text = "Settings"
        settingsTab.content = settings
        settingsTab.isClosable = false

        val requestPane = VBox()
        requestPane.children.add(taskbarPane)
        requestPane.children.add(request)
        val requestTab = Tab()
        requestTab.text = "Request"
        requestTab.content = requestPane
        requestTab.isClosable = false

        tabs.add(requestTab);
        tabs.add(settingsTab);
        side = Side.BOTTOM
    }

    fun initialize() {
        val taskbarView = FluentViewLoader.javaView(TaskbarView::class.java).load()

        taskbarPane.children.add(taskbarView.view)

        request.text = model!!.content.get()
        model!!.content.bind(request.textProperty())

    }
}
package eu.khaos.client.javafx

import javafx.application.Application
import javafx.stage.Stage

fun main(args: Array<String>) {
    Application.launch(KhaosApplication::class.java)
}

class KhaosApplication : Application() {

    override fun start(primaryStage: Stage) {
        primaryStage.title = "2048"
        val model = KhaosAppModel()
        val view = KhaosAppView(model)
        primaryStage.scene = view.asScene()
        primaryStage.show()
    }
}

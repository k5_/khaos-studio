package eu.khaos.studio.fx.environment

import de.saxsys.mvvmfx.InjectScope
import de.saxsys.mvvmfx.ViewModel
import eu.khaos.studio.environment.*
import eu.khaos.studio.fx.caller.Caller
import eu.khaos.studio.fx.nav.NavEnvironmentViewModel
import eu.khaos.studio.service.Environment
import javafx.beans.property.SimpleObjectProperty
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javax.inject.Inject

class EnvironmentViewModel @Inject constructor(
        private val caller: Caller<Environment>
) : ViewModel {

    @InjectScope
    var scope: EnvironmentScope? = null

    var saveAction: EventHandler<ActionEvent>? = null

    init {

        saveAction = EventHandler { save() }
    }

    fun save() {

        scope!!.environment.commit()
        val request = UpdateEnvironmentRequest()
        request.request = CTUpdateEnvironmentRequest()
        request.request.environment = scope!!.environment.get()
        request.request.environment.parent = null
        caller.success(this::saveSuccess).call().updateEnvironment(request)
    }

    fun saveSuccess(response: UpdateEnvironmentResponse?) {


        println("Save success")
    }

    private fun createRequest(): GetEnvironmentsRequest {
        val request = OBJECT_FACTORY.createGetEnvironmentsRequest()
        request.request = OBJECT_FACTORY.createCTGetEnvironmentsRequest()
        return request
    }

    companion object {
        private val OBJECT_FACTORY = ObjectFactory()
    }
}
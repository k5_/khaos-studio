package eu.khaos.studio.runner.control

import com.sun.jndi.toolkit.url.Uri
import eu.khaos.studio.runner.model.HttpRequest
import eu.khaos.studio.runner.model.HttpResponse
import org.apache.http.HttpEntity
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.DefaultHttpClient
import sun.misc.IOUtils
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.net.URI
import javax.ws.rs.client.Entity

class WsdlRequestRunner(
        private val run: HttpRequest
) {


    fun go() {

        val client = DefaultHttpClient()
        val post = HttpPost()
        post.uri = URI(run.uri)
        post.entity = StringEntity(run.body, run.mimeType, "UTF-8")

        val response = client.execute(post)


        val bytes = response.entity.content.use {
            read(it)
        }



        val httpResponse = HttpResponse(content = bytes)
        println(String(bytes))

    }


    private fun read(input: InputStream): ByteArray {
        val baos = ByteArrayOutputStream()
        val buffer = ByteArray(8192)

        do {
            val size = input.read(buffer)
            if (size > 0) {
                baos.write(buffer, 0, size)
            }
        } while (size > 0)
        return baos.toByteArray()
    }
}
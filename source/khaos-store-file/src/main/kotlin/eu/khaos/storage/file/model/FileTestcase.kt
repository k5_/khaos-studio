package eu.khaos.storage.file.model

import java.nio.file.Path
import javax.xml.bind.annotation.*

@XmlRootElement(name = "testcase")
@XmlAccessorType(XmlAccessType.NONE)
class FileTestcase {

    @XmlElement(name = "name")
    var name: String? = null


    @XmlElement(name = "properties")
    var properties: FileProperties? = null

    @XmlTransient
    var path: Path? = null

}
package eu.khaos.studio.runner.model

class HttpRequest(

        val method: RequestMethod = RequestMethod.POST,

        val mimeType: String,



        val body: String,

        var uri: String


)

enum class RequestMethod {
    POST
}
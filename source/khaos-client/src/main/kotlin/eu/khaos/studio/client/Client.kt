package eu.khaos.studio.client

import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.Response

interface Client {


    fun target(endpoint: String): WebTarget {
        return ClientBuilder.newClient().target(endpoint)
    }

    fun <T> handle(response: Response, type: Class<T>): T {
        if (response.status.div(100) == 2) {
            return response.readEntity(type)
        } else {
            val error = response.readEntity(ClientException.ErrorEntity::class.java)
            throw ClientException(error.message!!, error.code!!)
        }
    }
}
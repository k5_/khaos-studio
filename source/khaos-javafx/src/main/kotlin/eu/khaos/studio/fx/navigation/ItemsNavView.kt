package eu.khaos.studio.fx.navigation

import eu.khaos.studio.fx.Icons
import javafx.application.Platform
import javafx.collections.ListChangeListener
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox

class ItemsNavView(val model: NavigationModel) {

    private val main = VBox()

    private val actionbar = HBox()

    private val projectsRoot = TreeItem<ProjectItem>()

    private val projects = TreeView<ProjectItem>(projectsRoot);


    init {

        val addButton = Button("", Icons.ADD.get())

        addButton.onAction = EventHandler {
            model.addItem.set(!model.addItem.get())
        }
        actionbar.children.addAll(addButton)

        model.items!!.addListener(MyListChangeListener(projectsRoot))
        projects.isShowRoot = false
        projects.selectionModel.selectedItemProperty().addListener { observable, oldValue, newValue ->
            model.selected.set(newValue.value)
        }
        projects.onMouseClicked = MyMouseEventHandler(model)

        main.children.addAll(actionbar, projects)
    }


    fun asNode(): Node {
        return main
    }


    class MyMouseEventHandler(val model: NavigationModel) : EventHandler<MouseEvent> {

        override fun handle(event: MouseEvent) {
            if (event.button == MouseButton.PRIMARY && event.clickCount == 2) {
                model.openItem.set(model.selected.get())
            }
        }
    }

    class MyListChangeListener(private val rootNode: TreeItem<ProjectItem>) : ListChangeListener<ProjectItem> {
        override fun onChanged(c: ListChangeListener.Change<out ProjectItem>?) {
            if (c == null) {
                return
            }
            val addedList = ArrayList<TreeItem<ProjectItem>>()

            while (c.next()) {
                if (c.wasAdded()) {
                    for (added in c.addedSubList) {
                        val icon: Node? = Icons.getByItemType(added.type.get())

                        val item = TreeItem(added, icon)
                        added.children.addListener(MyListChangeListener(item))
                        addedList.add(item)
                    }
                }
            }
            Platform.runLater {
                rootNode.children.addAll(addedList)
            }
        }
    }


}
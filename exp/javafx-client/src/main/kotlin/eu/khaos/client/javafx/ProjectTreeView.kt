package eu.khaos.client.javafx

import javafx.scene.Node
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView

class ProjectTreeView(model: KhaosAppModel) {

    private val rootNode = TreeItem<String>()

    private val treeView = TreeView<String>(rootNode);

    fun asNode(): Node {
        return treeView
    }

}
package eu.khaos.studio.fx.utils

import de.saxsys.mvvmfx.utils.mapping.ModelWrapper
import de.saxsys.mvvmfx.utils.mapping.accessorfunctions.ObjectGetter
import de.saxsys.mvvmfx.utils.mapping.accessorfunctions.ObjectSetter
import de.saxsys.mvvmfx.utils.mapping.accessorfunctions.StringGetter
import de.saxsys.mvvmfx.utils.mapping.accessorfunctions.StringSetter
import javafx.beans.binding.Binding
import javafx.beans.binding.ObjectBinding
import javafx.beans.property.ObjectProperty
import javafx.beans.property.StringProperty
import javafx.beans.value.ObservableValue
import javafx.scene.control.TreeItem
import java.time.LocalDate


class Bind {


    companion object {

        fun <T, U> map(src: ObservableValue<T?>, func: (T) -> U): Binding<U?> {
            return object : ObjectBinding<U>() {
                init {
                    bind(src)
                }

                override fun computeValue(): U? {
                    val baseVal = src.getValue()
                    if (baseVal == null) {
                        return baseVal
                    }
                    return func.invoke(baseVal)
                }

                override fun dispose() {
                    unbind(src)
                }
            }

        }

        fun <T> treeItem(src: ObservableValue<TreeItem<T?>>): Binding<T?> {
            return object : ObjectBinding<T>() {
                init {
                    bind(src)
                }

                override fun computeValue(): T? {
                    val baseVal: TreeItem<T?>? = src.getValue()
                    return baseVal?.value
                }

                override fun dispose() {
                    unbind(src)
                }
            }
        }


    }
}


fun <M, P> ModelWrapper<M>.bean(id: String, getter: (M) -> (P?), setter: (M, P?) -> Unit): ObjectProperty<P?> {
    return this.field(id,
            ObjectGetter<M, P?>() { getter.invoke(it) },
            ObjectSetter<M, P?>() { m, s -> setter.invoke(m, s) }
    )
}


fun <M> ModelWrapper<M>.bean(id: String, getter: (M) -> (String?), setter: (M, String?) -> Unit): StringProperty {
    return this.field(id, StringGetter<M>() {
        getter.invoke(it) ?: ""
    }, StringSetter<M>() {
        m, s: String? -> setter.invoke(m, s)
    })
}
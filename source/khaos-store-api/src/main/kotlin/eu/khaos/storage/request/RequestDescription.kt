package eu.khaos.storage.request

import eu.khaos.storage.util.Properties

class RequestDescription {

    var name: String? = null

    var properties : Properties? = null

}

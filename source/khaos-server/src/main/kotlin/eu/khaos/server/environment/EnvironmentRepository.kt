package eu.khaos.server.environment

import eu.khaos.studio.environment.CTEnvironment
import java.nio.file.Path
import javax.inject.Singleton

@Singleton
class EnvironmentRepository(path: Path) {

    private val environments = HashMap<String, CTEnvironment>()

    init {
        val dev = createDev()
        val localhost = createLocalhost(dev)
        val acceptance = createAcceptance(dev)

        environments.put(dev.key, dev)
        environments.put(localhost.key, localhost)
        environments.put(acceptance.key, acceptance)
    }

    fun all(): List<CTEnvironment> {
        return ArrayList(environments.values)
    }


    private fun createDev(): CTEnvironment {
        val environment = CTEnvironment()
        environment.id = "_1"
        environment.key = "dev"
        environment.name = "dev"
        environment.isAbstract = true
        return environment
    }

    private fun createLocalhost(dev: CTEnvironment): CTEnvironment {
        val environment = CTEnvironment()
        environment.parent = dev
        environment.key = "localhost"
        environment.host = "http://localhost:8080"
        environment.name = "localhost"
        environment.isAbstract = false
        return environment
    }

    private fun createAcceptance(dev: CTEnvironment): CTEnvironment {
        val environment = CTEnvironment()
        environment.parent = dev
        environment.key = "acceptance"
        environment.host = "http://server:8080"
        environment.name = "acceptance"
        environment.isAbstract = false
        return environment
    }

    fun getByKey(key: String): CTEnvironment? {
        return environments.get(key)
    }
}
package eu.khaos.storage

class Project {

    var self: String? = null

    var projectId: String? = null

    var name: String? = null

    var code: String? = null

    var message: String? = null

}
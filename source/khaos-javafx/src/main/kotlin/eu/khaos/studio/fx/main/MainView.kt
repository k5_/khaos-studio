package eu.khaos.studio.fx.main

import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.JavaView
import eu.k5.arch.menu.MenuView
import eu.khaos.studio.fx.nav.NavigationView
import eu.khaos.studio.fx.tabs.TabsView
import javafx.scene.control.SplitPane
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import javax.swing.plaf.SplitPaneUI

class MainView : VBox(), JavaView<MainViewModel> {

    val splitPane = SplitPane()


    init {
        VBox.setVgrow(this, Priority.ALWAYS)
        splitPane.maxHeight = Double.MAX_VALUE


        VBox.setVgrow(splitPane, Priority.ALWAYS)

        children.add(splitPane)
    }


    fun initialize() {

        val menuView = FluentViewLoader.javaView(MenuView::class.java).load()
        children.add(0, menuView.view)


        val navView = FluentViewLoader.javaView(NavigationView::class.java).load()
        splitPane.items.add(0, navView.view)


        val tabsView = FluentViewLoader.javaView(TabsView::class.java).load()
        splitPane.items.add(1, tabsView.view)

    }
}
package eu.khaos.studio.fx.caller

import javafx.application.Platform
import java.lang.reflect.InvocationHandler
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import java.lang.reflect.Proxy
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


private val EXECUTORS: ExecutorService = Executors.newFixedThreadPool(5) { r ->
    val thread = Thread(r)
    thread.isDaemon = true
    thread
}

class CallerImpl<T>(
        private val type: Class<T>,
        private val instance: T
) : Caller<T> {

    override fun fail(): Caller<T> {
        return this
    }

    override fun <R> success(callback: (R?) -> Unit): CallerWithCallback<T, R> {
        return CallerWithCallbackImpl(type, instance, callback)
    }

}

class CallerWithCallbackImpl<T, R>(
        private val type: Class<T>,
        private val instance: T,
        private val callback: (R?) -> Unit
) : CallerWithCallback<T, R> {

    override fun fail(): CallerWithCallback<T, R> {
        return this
    }

    override fun call(): T {
        val types: Array<Class<T>> = Array(1) { type }

        val invoker = Invok<T, R>(instance, callback)

        val proxy = Proxy.newProxyInstance(Thread.currentThread().contextClassLoader, types, invoker)

        return type.cast(proxy)
    }


    private class Invok<T, R>(
            private val instance: T,
            private val callback: (R?) -> Unit
    ) : InvocationHandler {
        override fun invoke(proxy: Any?, method: Method, args: Array<out Any>?): Any? {
            // Threading
            val result = EXECUTORS.submit() {
                try {
                    val ret: Any? = method.invoke(instance, args!![0])
                    Platform.runLater() {
                        if (ret == null) {
                            callback(null)
                        } else {
                            callback(ret as R)
                        }
                    }
                } catch (e: InvocationTargetException) {
                    e.printStackTrace()
                } catch (e: Throwable) {
                    e.printStackTrace()
                }
            }
            return null
        }
    }

}


package eu.khaos.studio.fx.nav

import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.JavaView
import eu.khaos.studio.fx.main.MainView
import javafx.scene.Scene
import javafx.scene.control.TitledPane
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox

class NavigationView : VBox(), JavaView<NavigationViewModel> {

    init {

    }

    fun initialize() {
        val navItems = FluentViewLoader.javaView(NavItemsView::class.java).load()

        val library = FluentViewLoader.javaView(NavLibraryView::class.java).load()

        val environment = FluentViewLoader.javaView(NavEnvironmentView::class.java).load()



        VBox.setVgrow(this, Priority.ALWAYS)
        VBox.setVgrow(navItems.view, Priority.ALWAYS)
        VBox.setVgrow(library.view, Priority.NEVER)
        VBox.setVgrow(environment.view, Priority.NEVER)

        this.maxHeight = Double.MAX_VALUE
        children.addAll(navItems.view, library.view, environment.view)

    }
}
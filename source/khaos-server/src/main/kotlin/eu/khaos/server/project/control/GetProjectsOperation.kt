package eu.khaos.server.project.control

import eu.khaos.server.environment.AbstractOperation
import eu.khaos.storage.ItemType
import eu.khaos.storage.file.boundary.StorageRepository
import eu.khaos.studio.storage.*
import javax.inject.Inject

class GetProjectsOperation @Inject constructor(
        private val repository: StorageRepository
) : AbstractOperation<CTGetProjectsRequest, CTGetProjectsResponse>(
        OBJECT_FACTORY::createCTGetProjectsResponse
) {

    override fun invoke(request: CTGetProjectsRequest): CTGetProjectsResponse {
        val projects = repository.listProjects()
        val ctResponse = success()

        for (projectItem in projects) {
            val ctProjectItemRef = OBJECT_FACTORY.createCTProjectItemRef()
            ctProjectItemRef.name = projectItem.name
            ctProjectItemRef.key = projectItem.path
            if (projectItem.type == ItemType.PROJECT) {
                ctProjectItemRef.type = ProjectItemType.PROJECT
            } else if (projectItem.type == ItemType.FOLDER) {
                ctProjectItemRef.type = ProjectItemType.FOLDER
            }

            ctResponse.item.add(ctProjectItemRef)
        }
        return ctResponse
    }

    companion object {
        val OBJECT_FACTORY = ObjectFactory()
    }
}
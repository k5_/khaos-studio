package eu.khaos.server.project.boundary

import eu.khaos.server.project.control.GetItemsOperation
import eu.khaos.server.project.control.GetProjectsOperation
import eu.khaos.server.project.control.GetWsdlRequestOperation
import eu.khaos.studio.service.Storage
import eu.khaos.studio.storage.*
import javax.inject.Inject

class StorageService @Inject constructor(
        private val getWsdlRequest: GetWsdlRequestOperation,
        private val getProjects: GetProjectsOperation,
        private val getItems: GetItemsOperation
) : Storage {

    override fun getProjects(body: GetProjectsRequest): GetProjectsResponse {
        val response = OBJECT_FACTORY.createGetProjectsResponse()
        response.response = getProjects.invoke(body.request)
        return response
    }

    override fun getWsdlRequest(body: GetWsdlRequestRequest): GetWsdlRequestResponse {
        val response = OBJECT_FACTORY.createGetWsdlRequestResponse()
        response.response = getWsdlRequest.invoke(body.request)
        return response
    }

    override fun getItems(body: GetItemsRequest): GetItemsResponse {
        val response = OBJECT_FACTORY.createGetItemsResponse()
        response.response = getItems.invoke(body.request)
        return response
    }

    companion object {
        private val OBJECT_FACTORY = ObjectFactory()
    }
}
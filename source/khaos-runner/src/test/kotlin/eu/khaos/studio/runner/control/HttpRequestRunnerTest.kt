package eu.khaos.studio.runner.control

import eu.khaos.studio.runner.model.HttpRequest
import org.junit.Test


class HttpRequestRunnerTest {


    @Test
    fun test() {
        val request = HttpRequest(body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:stoc=\"http://example.com/stockquote.xsd\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <stoc:TradePriceRequest>\n" +
                "         <tickerSymbol>?</tickerSymbol>\n" +
                "      </stoc:TradePriceRequest>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>", uri = "http://localhost:8080/soap/hello", mimeType = "application/xml")

        val runner = WsdlRequestRunner(request)

        runner.go()
    }

}
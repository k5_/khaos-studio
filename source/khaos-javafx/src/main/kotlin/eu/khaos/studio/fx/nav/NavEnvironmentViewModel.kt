package eu.khaos.studio.fx.nav

import de.saxsys.mvvmfx.Initialize
import de.saxsys.mvvmfx.ViewModel
import eu.khaos.studio.environment.CTEnvironment
import eu.khaos.studio.environment.GetEnvironmentsRequest
import eu.khaos.studio.environment.GetEnvironmentsResponse
import eu.khaos.studio.environment.ObjectFactory
import eu.khaos.studio.fx.OpenItemEvent
import eu.khaos.studio.fx.caller.Caller
import eu.khaos.studio.fx.navigation.ProjectItemType
import eu.khaos.studio.service.Environment
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javax.enterprise.event.Event
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NavEnvironmentViewModel @Inject constructor(
        private val caller: Caller<Environment>,
        private val open: Event<OpenItemEvent>
) : ViewModel {

    val allEnvironments = HashMap<String, FxEnvironment>()
    val rootEnvironments = FXCollections.observableArrayList<FxEnvironment>()

    val selected = SimpleObjectProperty<FxEnvironment?>()


    @Initialize
    fun ini() {
        println("init")
        caller.success(this::load).call().getEnvironments(createRequest())
    }

    fun openCurrent() {
        val current = selected.get()
        println("open current environment")
        if (current != null) {
            val event = OpenItemEvent(current.env.get().name, "",current.key,  ProjectItemType.ENVIRONMENT, current.env.get())
            open.fire(event)
        }
    }

    private fun createRequest(): GetEnvironmentsRequest? {
        val request = OBJECT_FACTORY.createGetEnvironmentsRequest()
        request.request = OBJECT_FACTORY.createCTGetEnvironmentsRequest()
        return request
    }

    private fun load(response: GetEnvironmentsResponse?) {
        val ctResponse = response!!.response

        for (ctEnvironment in ctResponse.environment) {
            println(ctEnvironment.key)
            loadEnvironment(ctEnvironment)
        }
        println("test")
    }

    private fun loadEnvironment(env: CTEnvironment): FxEnvironment? {
        val byKey = getByKey(env.key)
        if (byKey != null) {
            return byKey
        }
        val list: MutableList<FxEnvironment>
        if (env.parent != null) {
            list = loadEnvironment(env.parent as CTEnvironment)!!.children
        } else {
            list = rootEnvironments
        }
        val fxEnv = createEnvironment(env)
        list.add(fxEnv)
        allEnvironments.put(env.key, fxEnv)
        return fxEnv
    }

    private fun getByKey(key: String): FxEnvironment? {
        return allEnvironments.get(key)
    }

    private fun createEnvironment(ctEnvironment: CTEnvironment): FxEnvironment {
        val env = FxEnvironment(ctEnvironment.key)
        env.env.set(ctEnvironment)
        return env
    }

    companion object {
        private val OBJECT_FACTORY = ObjectFactory()
    }
}


class FxEnvironment(
        val key: String
) {
    val env = SimpleObjectProperty<CTEnvironment>()
    val children = FXCollections.observableArrayList<FxEnvironment>()


    override fun toString(): String {
        return env.get().name
    }
}
package eu.khaos.studio.fx.caller

interface Caller<T> {

    fun <R> success(callback: (R?) -> Unit): CallerWithCallback<T, R>

    fun fail(): Caller<T>

}

interface CallerWithCallback<T, R> {

    fun fail(): CallerWithCallback<T, R>

    fun call(): T
}
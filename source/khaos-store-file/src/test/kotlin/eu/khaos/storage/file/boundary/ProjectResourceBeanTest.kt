package eu.khaos.storage.file.boundary

import eu.khaos.storage.AddWsdlRequest
import eu.khaos.storage.CreateProject
import eu.khaos.storage.wsdl.AddWsdl
import org.junit.Test

class ProjectResourceBeanTest {

    @Test
    fun createProject() {
        TestStore.tempRepository().createProject(CreateProject(name = "abc"))
    }

    @Test
    fun addWsdl() {
        val repository = TestStore.tempRepository()
        val p = repository.createProject(CreateProject(name = "abc"))

        val projectResouce = repository.project(p.self!!).addWsdl(AddWsdl(name = "Test"))
    }

    @Test
    fun addRequest() {
        val repository = TestStore.tempRepository()
        val p = repository.createProject(CreateProject(name = "abc"))

      //  repository.storage(p.self!!).addRequest(AddWsdlRequest(name = "test"))

    }


}
package eu.khaos.storage.file.model

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "folder")
@XmlAccessorType(XmlAccessType.NONE)
class FileFolder {

    @XmlElement(name = "name")
    var name: String? = null


}
package eu.k5.arch.menu

import de.saxsys.mvvmfx.JavaView
import javafx.event.EventHandler
import javafx.scene.control.Menu
import javafx.scene.control.MenuBar
import javafx.scene.control.MenuItem
import eu.k5.arch.about.AboutView
import de.saxsys.mvvmfx.FluentViewLoader
import eu.k5.arch.DialogHelper
import javafx.stage.Stage
import javax.inject.Inject


class MenuView : MenuBar(), JavaView<MenuViewModel> {

    val fileMenu = Menu("File")

    val helpMenu: Menu


    @Inject
    var primaryStage: Stage? = null


    @Inject
    var dialog: DialogHelper? = null

    init {
        helpMenu = createHelpMenu(this)

        menus.addAll(fileMenu, helpMenu)
    }


    fun about() {
        val view = FluentViewLoader.javaView(AboutView::class.java).load().getView()
        dialog?.show(view, primaryStage)

    }

    companion object {
        fun createHelpMenu(view: MenuView): Menu {

            val menu = Menu("help")

            val aboutMenuItem = MenuItem("About")
            aboutMenuItem.onAction = EventHandler { view.about() }

            menu.items.addAll(aboutMenuItem)
            return menu
        }
    }
}
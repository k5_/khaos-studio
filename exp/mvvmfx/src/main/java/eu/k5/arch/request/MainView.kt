package eu.k5.arch.request

import de.saxsys.mvvmfx.*
import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import javafx.scene.layout.VBox
import java.util.function.BiConsumer

class MainView : VBox(), JavaView<MainViewModel> {


    @InjectContext
    private var context: Context? = null

    @InjectViewModel
    private var viewModel: MainViewModel? = null

    private val open = Button("open")

    private val tabPane = TabPane()

    init {
        children.addAll(open, tabPane)
    }

    fun initialize() {
        viewModel?.onOpenDocument(BiConsumer { title, scopes ->


            val root = FluentViewLoader.javaView(RequestView::class.java)
                    .context(context)
                    .providedScopes(scopes)
                    .load()
                    .getView()

            val tab = Tab()
            tab.setText(title)
            tab.setClosable(true);
            tab.setContent(root);


            tabPane.getTabs().add(tab)
        })
        open.onAction = EventHandler { viewModel!!.openRequest("xx") }
        println("init")
    }

}
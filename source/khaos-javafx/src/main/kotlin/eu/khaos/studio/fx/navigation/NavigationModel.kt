package eu.khaos.studio.fx.navigation

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList

class NavigationModel {

    val root = ProjectItem("")

    val items: ObservableList<ProjectItem> = root.children

    val addItem = SimpleBooleanProperty(false)

    val selected = SimpleObjectProperty<ProjectItem?>()

    val openItem = SimpleObjectProperty<ProjectItem?>()

}

class ProjectItem(val localPath: String) {
    val name = SimpleStringProperty()

    val id = SimpleStringProperty()

    val project = SimpleStringProperty()

    val type = SimpleObjectProperty<ProjectItemType>(ProjectItemType.UNKNOWN)

    val children: ObservableList<ProjectItem> = FXCollections.observableArrayList()
    val path = SimpleStringProperty()

    override fun toString(): String {
        return name.get()
    }

    fun getChild(localPath: String): ProjectItem? {
        for (child in children) {
            if (child.localPath == localPath) {
                return child
            }
        }
        return null
    }
}

enum class ProjectItemType {
    UNKNOWN, FOLDER, PROJECT, REQUEST, ENVIRONMENT
}
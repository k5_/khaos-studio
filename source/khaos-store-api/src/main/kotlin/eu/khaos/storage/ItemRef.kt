package eu.khaos.storage

class ItemRef {
    var path: String? = null
    var name: String? = null
    var type: ItemType = ItemType.UNKNOWN
    var parent: ItemRef? = null
    var self: String? = null
    var id: String? = null
}

enum class ItemType {
    UNKNOWN, FOLDER, PROJECT, REQUEST, TESTCASE
}
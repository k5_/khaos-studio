package eu.khaos.studio.fx.navigation.dialog

import eu.khaos.studio.fx.Action
import eu.khaos.studio.fx.navigation.ProjectItem

class CreateDialog(

        val parent: ProjectItem

) {

    private val model = CreateDialogModel()
    private val view = CreateDialogView(model)

    var requestCreateAction: ((CreateDialogModel) -> Unit)? = null

    var folderCreateAction: ((CreateDialogModel) -> Unit)? = null

    var testcaseCreateAction: ((CreateDialogModel) -> Unit)? = null

    init {

        model.folderClicked.addListener { _, _, _ -> folderCreateAction?.invoke(model); close() }
        model.requestClicked.addListener { _, _, _ -> requestCreateAction?.invoke(model); close() }
        model.testcaseClicked.addListener { _, _, _ -> testcaseCreateAction?.invoke(model); close() }

    }

    fun display() {
        view.display()
    }

    fun close() {
        view.close()
    }

}
package eu.khaos.studio.fx.tabs

import de.saxsys.mvvmfx.Scope
import de.saxsys.mvvmfx.ViewModel
import eu.khaos.studio.environment.CTEnvironment
import eu.khaos.studio.fx.OpenItemEvent
import eu.khaos.studio.fx.environment.EnvironmentScope
import eu.khaos.studio.fx.navigation.ProjectItemType
import eu.khaos.studio.fx.wsdlrequest.WsdlRequestScope
import java.util.*
import java.util.function.BiConsumer
import java.util.function.BiFunction
import javax.enterprise.event.Observes
import javax.inject.Singleton
import kotlin.collections.ArrayList

@Singleton
class TabsViewModel : ViewModel {


    var openRequest: ((String, List<Scope>) -> Unit)? = null

    var openEnvironment: ((String, List<Scope>) -> Unit)? = null

    fun openItem(@Observes event: OpenItemEvent) {
        println("open " + event.name + " " + event.path)

        if (event.type == ProjectItemType.REQUEST) {
            openRequest(event)
        } else if (event.type == ProjectItemType.ENVIRONMENT) {
            openEnvironments(event)
        } else {
            println((event.type?.name ?: "") + " " + event.path)
        }
    }

    private fun openRequest(event: OpenItemEvent) {
        val scope = WsdlRequestScope(event.project, event.path)
        openRequest!!.invoke(event.name, Arrays.asList(scope))
    }

    private fun openEnvironments(event: OpenItemEvent) {

        val scope = EnvironmentScope()
        scope.environment.set(event.instance as CTEnvironment)
        openEnvironment!!.invoke("Environments", Arrays.asList(scope))
    }
}
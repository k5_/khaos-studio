package eu.khaos.server.library.control

import eu.khaos.server.environment.AbstractOperation
import eu.khaos.studio.library.CTImportLibraryRequest
import eu.khaos.studio.library.CTImportLibraryResponse
import eu.khaos.studio.library.ObjectFactory

class ImportLibraryOperation
    : AbstractOperation<CTImportLibraryRequest, CTImportLibraryResponse>(
        ObjectFactory()::createCTImportLibraryResponse
) {


    override fun invoke(request: CTImportLibraryRequest): CTImportLibraryResponse {

        TODO("implement")
    }

}
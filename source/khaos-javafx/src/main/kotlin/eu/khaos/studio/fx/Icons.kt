package eu.khaos.studio.fx

import eu.khaos.studio.fx.navigation.ProjectItem
import eu.khaos.studio.fx.navigation.ProjectItemType
import javafx.scene.Node
import javafx.scene.image.Image
import javafx.scene.image.ImageView

enum class Icons(name: String) {
    ADD("add.png"),

    PROJECT("storage.png"),

    FOLDER("folder.png"),

    SOAP("soap.png");

    val image: Image


    init {
        val stream = Icons::class.java.getResourceAsStream(name)
        image = stream.use { Image(it) }

    }

    fun get(): Node {
        return ImageView(image)
    }


    companion object {

        fun getByItemType(type: ProjectItemType): Node? {
            if (type == ProjectItemType.PROJECT) {
                return Icons.PROJECT.get()
            } else if (type == ProjectItemType.FOLDER) {
                return Icons.FOLDER.get()
            } else if (type == ProjectItemType.REQUEST) {
                return Icons.SOAP.get()
            } else {
                return null
            }
        }
    }

}
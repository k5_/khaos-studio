package eu.khaos.studio.fx.environment

import de.saxsys.mvvmfx.Initialize
import de.saxsys.mvvmfx.InjectScope
import de.saxsys.mvvmfx.ViewModel
import de.saxsys.mvvmfx.utils.mapping.ModelWrapper
import de.saxsys.mvvmfx.utils.mapping.accessorfunctions.StringGetter
import de.saxsys.mvvmfx.utils.mapping.accessorfunctions.StringSetter
import eu.khaos.studio.base.CTProperties
import eu.khaos.studio.environment.CTEnvironment
import eu.khaos.studio.fx.caller.Caller
import eu.khaos.studio.fx.utils.bean
import eu.khaos.studio.fx.utils.properties.PropertiesScope
import eu.khaos.studio.service.Environment
import javafx.beans.property.StringProperty
import javax.inject.Inject


class EditEnvironmentViewModel @Inject constructor(
        private val caller: Caller<Environment>
) : ViewModel {


    @InjectScope
    var scope: EnvironmentScope? = null

    val propertiesScope: PropertiesScope = PropertiesScope()

    @Initialize
    fun ini() {
        println("Model initialized")
        val ctEnv = scope!!.environment.get()

        if (ctEnv.properties == null) {
            ctEnv.properties = CTProperties()
        }
        propertiesScope.properties.set(ctEnv.properties)
    }


    fun nameProperty(): StringProperty {
        return scope!!.environment.bean("name", CTEnvironment::getName, CTEnvironment::setName)
    }

    fun hostProperty(): StringProperty {
        return scope!!.environment.bean("host", CTEnvironment::getHost, CTEnvironment::setHost)
    }

}

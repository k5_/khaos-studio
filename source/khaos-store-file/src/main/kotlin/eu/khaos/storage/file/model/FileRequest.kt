package eu.khaos.storage.file.model

import java.nio.file.Path
import javax.xml.bind.annotation.*

@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.NONE)
class FileRequest {

    @XmlElement(name = "name")
    var name: String? = null

    @XmlElement(name = "operation")
    val operation: String? = null

    @XmlElement(name = "properties")
    var properties: FileProperties? = null

    @XmlTransient
    var path: Path? = null

}
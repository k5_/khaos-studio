package eu.khaos.server.library.boundary

import eu.khaos.server.library.control.GetLibrariesOperation
import eu.khaos.server.library.control.ImportLibraryOperation
import eu.khaos.studio.library.ObjectFactory
import eu.khaos.studio.library.GetLibrariesRequest
import eu.khaos.studio.library.GetLibrariesResponse
import eu.khaos.studio.library.ImportLibraryRequest
import eu.khaos.studio.library.ImportLibraryResponse
import eu.khaos.studio.service.Library
import javax.inject.Inject

class LibraryService @Inject constructor(
        private val importLibrary: ImportLibraryOperation,
        private val getLibraries: GetLibrariesOperation
) : Library {

    override fun importLibrary(body: ImportLibraryRequest): ImportLibraryResponse {
        val response = OBJECT_FACTORY.createImportLibraryResponse()
        response.response = importLibrary.invoke(body.request)
        return response
    }

    override fun getLibraries(body: GetLibrariesRequest): GetLibrariesResponse {
        val response = OBJECT_FACTORY.createGetLibrariesResponse()
        response.response = getLibraries.invoke(body.request)
        return response
    }

    companion object {
        private val OBJECT_FACTORY = ObjectFactory()
    }
}
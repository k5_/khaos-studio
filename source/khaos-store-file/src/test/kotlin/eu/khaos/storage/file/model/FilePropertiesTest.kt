package eu.khaos.storage.file.model

import eu.khaos.storage.file.control.FileModel
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals

class FilePropertiesTest {

    @Test
    fun test() {

        val fileProperties = FileProperties()

        val newProperty = FileProperties.FileProperty()
        newProperty.key = "key"
        newProperty.value = "value"
        fileProperties.property = ArrayList()
        fileProperties.property!!.add(newProperty)

        val newFileProperties = FileModel().fromXml(FileModel.toXml(fileProperties), FileProperties::class.java)


        val asProperties = newFileProperties.asProperties()

        assertEquals(1, asProperties.property!!.size)

        assertEquals("key", asProperties.property!![0].key)
        assertEquals("value", asProperties.property!![0].value)
    }

}
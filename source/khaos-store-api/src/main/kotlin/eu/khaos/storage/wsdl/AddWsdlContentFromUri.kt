package eu.khaos.storage.wsdl

class AddWsdlContentFromUri {
    var uri: String? = null
    var beautify: Boolean? = false
    var validate: Boolean? = false
}
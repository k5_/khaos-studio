package eu.khaos.studio.fx.nav

import de.saxsys.mvvmfx.InjectViewModel
import de.saxsys.mvvmfx.JavaView
import eu.khaos.studio.fx.Icons
import eu.khaos.studio.fx.utils.Bind
import javafx.collections.ListChangeListener
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.TitledPane
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.input.MouseButton
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox

class NavEnvironmentView constructor(


) : TitledPane(), JavaView<NavEnvironmentViewModel> {

    private val main = VBox()

    private val actionbar = HBox()

    private val envRoot = TreeItem<FxEnvironment>()

    private val envs = TreeView<FxEnvironment>(envRoot);

    @InjectViewModel
    private var model: NavEnvironmentViewModel? = null

    init {
        text = "Environment"
        main.children.addAll(actionbar, envs)

        envs.isShowRoot = false
        content = main
        isExpanded = false
    }


    fun initialize() {
        println("ini")
        addEnv(envRoot, model!!.rootEnvironments)
        model!!.rootEnvironments.addListener(NewEnvironmentsChangeListener(envRoot))
        model!!.selected.bind(Bind.map(envs.selectionModel.selectedItemProperty()) { it.value })

        envs.onMouseClicked = EventHandler {
            if (it.button == MouseButton.PRIMARY && it.clickCount == 2) {
                model!!.openCurrent()
            }
        }

    }

    private fun addEnv(rootNode: TreeItem<FxEnvironment>, addEnvironments: MutableList<out FxEnvironment>) {
        for (added in addEnvironments) {
            val icon: Node? = Icons.ADD.get()
            val item = TreeItem(added, icon)
            added.children.addListener(NewEnvironmentsChangeListener(item))
            rootNode.children.add(item)
        }
    }

    inner class NewEnvironmentsChangeListener(private val rootNode: TreeItem<FxEnvironment>) : ListChangeListener<FxEnvironment> {
        override fun onChanged(c: ListChangeListener.Change<out FxEnvironment>?) {
            if (c == null) {
                return
            }

            while (c.next()) {
                if (c.wasAdded()) {
                    addEnv(rootNode, c.addedSubList)
                }
            }
        }
    }


}
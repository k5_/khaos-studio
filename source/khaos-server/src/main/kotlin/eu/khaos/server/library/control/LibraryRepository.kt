package eu.khaos.server.library.control

import eu.khaos.server.library.model.FileLibrary
import java.nio.file.Files
import java.nio.file.Path

class LibraryRepository(val path: Path) {


    fun all(): List<FileLibrary> {
        val librariesPath = path.resolve("_libraries")

        val list = ArrayList<FileLibrary>()
        Files.list(librariesPath).forEach {
            if (Files.isDirectory(it)) {
                list.add(loadLibraryDescription(it.resolve("library.description.xml")))
            }
        }
        return list
    }

    private fun loadLibraryDescription(path: Path): FileLibrary {
        return LibraryModel.loadLibrary(path)
    }

}
package eu.khaos.studio.fx

import eu.khaos.studio.fx.navigation.ProjectItemType

class OpenItemEvent(
        val name: String,
        val project: String,
        val path: String,
        val type: ProjectItemType,
        var instance: Any? = null
)
package eu.khaos.storage.file.model

import javax.xml.bind.annotation.*

@XmlAccessorType(XmlAccessType.NONE)
class FileWsdl {

    @XmlElement(name = "name")
    var name: String? = null

}
package eu.khaos.server.library.control

import eu.khaos.server.environment.AbstractOperation
import eu.khaos.server.library.model.FileInterface
import eu.khaos.server.library.model.FileLibrary
import eu.khaos.server.library.model.FileOperation
import eu.khaos.studio.library.*
import javax.inject.Inject

class GetLibrariesOperation @Inject constructor(
        private val repository: LibraryRepository
) : AbstractOperation<CTGetLibrariesRequest, CTGetLibrariesResponse>(
        ObjectFactory()::createCTGetLibrariesResponse
) {

    override fun invoke(request: CTGetLibrariesRequest): CTGetLibrariesResponse {
        val ctResponse = OBJECT_FACTORY.createCTGetLibrariesResponse()
        for (fileLibrary in repository.all()) {

            ctResponse.library.add(mapLibrary(fileLibrary))

        }
        return ctResponse
    }

    private fun mapLibrary(fileLibrary: FileLibrary): CTLibrary {

        val library = OBJECT_FACTORY.createCTLibrary()
        library.name = fileLibrary.name

        for (fileInterface in fileLibrary.`interface`) {
            library.`interface`.add(mapInterface(fileInterface))
        }
        return library
    }

    private fun mapInterface(fileInterface: FileInterface): CTInterface {
        val ctInterface = OBJECT_FACTORY.createCTInterface()
        ctInterface.name = fileInterface.name

        for (fileOperation in fileInterface.operation) {
            ctInterface.operation.add(mapOperation(fileOperation))
        }
        return ctInterface
    }

    private fun mapOperation(fileOperation: FileOperation): CTOperation {
        val ctOperation = OBJECT_FACTORY.createCTOperation()
        ctOperation.name = fileOperation.name
        return ctOperation
    }

    companion object {
        private val OBJECT_FACTORY = ObjectFactory()
    }

}
package eu.k5.arch.request

import de.saxsys.mvvmfx.*
import eu.k5.arch.HelloWorldView
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import java.net.URL
import java.util.*

class RequestView : HBox(), JavaView<RequestViewModel>, Initializable {


    @InjectViewModel
    var model: RequestViewModel? = null

    @InjectContext
    var context: Context? = null


    init {

    }


    override fun initialize(location: URL?, resources: ResourceBundle?) {

        val viewTuple = FluentViewLoader.javaView(TaskbarView::class.java)
                .context(context)
                .providedScopes(model!!.requestScope)
                .load()

        children.addAll(viewTuple.view)
    }

}
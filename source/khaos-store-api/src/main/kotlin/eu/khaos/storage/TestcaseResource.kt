package eu.khaos.storage

import javax.ws.rs.*
import javax.ws.rs.core.MediaType

interface TestcaseResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun get(): Testcase


    @PUT
    @Path("/items")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun createStep(step:CreateStep): ItemRef


}
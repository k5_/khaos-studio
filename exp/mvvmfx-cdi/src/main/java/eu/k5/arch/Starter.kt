package eu.k5.arch

import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.cdi.MvvmfxCdiApplication
import javafx.application.Application
import javafx.scene.Scene
import javafx.stage.Stage

fun main(args: Array<String>) {
    Application.launch(App::class.java)
}

class App : MvvmfxCdiApplication() {
    override fun startMvvmfx(stage: Stage) {

        val main = FluentViewLoader.javaView(MainView::class.java).load()
        val rootScene = Scene(main.getView())

        stage.scene = rootScene
        stage.show();
    }

}
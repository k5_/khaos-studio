package eu.khaos.studio.fx.nav

import de.saxsys.mvvmfx.JavaView
import javafx.scene.control.TitledPane

class NavLibraryView : TitledPane(), JavaView<NavEnvironmentViewModel> {

    init {
        text = "Library"
    }
}
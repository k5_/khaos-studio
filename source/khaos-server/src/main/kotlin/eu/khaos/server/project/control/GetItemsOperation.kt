package eu.khaos.server.project.control

import eu.khaos.server.environment.AbstractOperation
import eu.khaos.storage.file.boundary.StorageRepository
import eu.khaos.studio.storage.*
import javax.inject.Inject

class GetItemsOperation @Inject constructor(
        private val repository: StorageRepository

) : AbstractOperation<CTGetItemsRequest, CTGetItemsResponse>(
        ObjectFactory()::createCTGetItemsResponse
) {

    override fun invoke(request: CTGetItemsRequest): CTGetItemsResponse {
        return success()
    }

}
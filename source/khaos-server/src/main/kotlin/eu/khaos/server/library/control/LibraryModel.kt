package eu.khaos.server.library.control

import eu.khaos.server.library.model.FileLibrary
import java.nio.file.Path
import javax.inject.Singleton
import javax.xml.bind.JAXBContext

object LibraryModel {

    private val context: JAXBContext

    init {
        context = JAXBContext.newInstance(FileLibrary::class.java)
    }

    fun loadLibrary(path: Path): FileLibrary {
        val unmarshal: Any? = context.createUnmarshaller().unmarshal(path.toFile())

        if (unmarshal is FileLibrary) {
            return unmarshal
        } else {
            throw IllegalArgumentException("Unsupported type in file" + path.toString())
        }
    }
}
package eu.khaos.storage.file.boundary

import javax.ws.rs.core.Response

class ConsumerException(message: String, val status: Response.Status) : RuntimeException(message) {

}
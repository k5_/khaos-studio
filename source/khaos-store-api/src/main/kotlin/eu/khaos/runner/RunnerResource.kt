package eu.khaos.runner

import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Path("runner")
interface RunnerResource {


    @Path("/runs")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun createRunner(create: CreateRun): RunRef


    @Path("/run/{id}")
    fun run(): RunResource

}
package eu.khaos.studio.client

import eu.khaos.storage.request.RequestDescription
import eu.khaos.storage.request.RequestResource
import javax.ws.rs.client.Entity
import javax.ws.rs.core.MediaType

class RequestClient(
        private var endpoint: String
) : RequestResource, Client {

    override fun get(): RequestDescription {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun post(update: RequestDescription): RequestDescription {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun content(): String {
        return target(endpoint).path("content").request().get(String::class.java)
    }

    override fun content(content: String) {
        val response = target(endpoint).path("content").request().post(Entity.entity(content, MediaType.APPLICATION_JSON_TYPE))
    }

}
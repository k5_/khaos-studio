package eu.khaos.storage.file.boundary

import eu.khaos.storage.request.RequestResource
import eu.khaos.storage.Project
import eu.khaos.storage.file.model.FileRequestRef
import eu.khaos.storage.request.RequestDescription
import java.nio.charset.StandardCharsets
import java.nio.file.Files

class RequestResourceBean(val request: FileRequestRef) : RequestResource {

    override fun get(): RequestDescription {


        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun post(update: RequestDescription): RequestDescription {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun content(): String {
        val contentPath = request.path.resolve("content.xml")

        if (Files.exists(contentPath)){
            val bytes = Files.readAllBytes(contentPath)
            return String(bytes, StandardCharsets.UTF_8)
        }
        return ""
    }

    override fun content(content: String) {
        val contentPath = request.path.resolve("content.xml")
        Files.write(contentPath, content.toByteArray(StandardCharsets.UTF_8))
    }

}
package eu.khaos.studio.fx.wsdlrequest

import eu.khaos.runner.CreateRun
import eu.khaos.storage.request.RequestResource
import eu.khaos.studio.client.KhaosClient
import eu.khaos.studio.fx.navigation.ProjectItem
import javafx.scene.Node
import javafx.scene.layout.Pane
import javax.swing.event.ChangeListener

class WsdlRequestControl(path: String) {

    private val model : WsdlRequestModel? = null
    private val view : WsdlRequestView
    private val client = KhaosClient("http://localhost:8080/api")
    private val request: RequestResource

    init {
        request = client.request(path)
        model!!.start.addListener { _, _, _ -> start() }
        model!!.save.addListener { _, _, _ -> save() }
        model!!.content.set(request.content())
        view =  WsdlRequestView ()
    }

    fun save() {
        println("save")
        request.content(model!!.content.get())
    }

    fun start() {
        println("Run")
        val runner = client.runner()
        val createRun = CreateRun()
        createRun.requestBody = model!!.content.value
        createRun.mimeType = "application/xml"
        createRun.uri = model!!.url.value

        runner.createRunner(createRun)
    }

    fun asNode(): Node {
        return Pane()
    }

}
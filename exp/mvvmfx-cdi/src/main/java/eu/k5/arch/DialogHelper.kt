package eu.k5.arch

import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage
import javafx.stage.StageStyle

class DialogHelper {

    fun show(view: Parent, parentStage: Stage?): Stage? {

        val dialogStage = Stage(StageStyle.UTILITY)
        dialogStage.initOwner(parentStage)
        if (dialogStage.scene == null) {
            val dialogScene = Scene(view)
            dialogStage.scene = dialogScene
            dialogStage.sizeToScene()
            dialogStage.show()
            return dialogStage
        }
        return null

    }
}
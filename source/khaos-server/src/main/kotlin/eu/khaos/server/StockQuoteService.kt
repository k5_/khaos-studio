package eu.khaos.server

import com.example.stockquote.TradePrice
import com.example.stockquote.TradePriceRequest
import com.example.stockquote_wsdl.StockQuotePortType

class StockQuoteService : StockQuotePortType {

    override fun getLastTradePrice(body: TradePriceRequest): TradePrice {

        val tradePrice = com.example.stockquote.ObjectFactory().createTradePrice()
        tradePrice.setPrice(1.5f)
        return tradePrice
    }
}
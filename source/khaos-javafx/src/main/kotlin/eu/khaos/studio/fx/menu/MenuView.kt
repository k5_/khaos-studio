package eu.k5.arch.menu

import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.JavaView
import eu.k5.arch.about.AboutView
import eu.khaos.studio.fx.utils.DialogHelper
import javafx.event.EventHandler
import javafx.scene.control.Menu
import javafx.scene.control.MenuBar
import javafx.scene.control.MenuItem
import javafx.stage.Stage
import javax.inject.Inject


class MenuView : MenuBar(), JavaView<MenuViewModel> {

    private val fileMenu: Menu

    private val helpMenu: Menu


    @Inject
    var primaryStage: Stage? = null


    @Inject
    var dialog: DialogHelper? = null

    init {
        fileMenu = createFileMenu(this)
        helpMenu = createHelpMenu(this)
        menus.addAll(fileMenu, helpMenu)
    }


    fun about() {
        val view = FluentViewLoader.javaView(AboutView::class.java).load().getView()
        dialog?.show(view, primaryStage)
    }

    fun close() {

    }

    companion object {
        private fun createHelpMenu(view: MenuView): Menu {
            val menu = Menu("help")

            val aboutMenuItem = MenuItem("About")
            aboutMenuItem.onAction = EventHandler { view.about() }

            menu.items.addAll(aboutMenuItem)
            return menu
        }

        private fun createFileMenu(view: MenuView): Menu {
            val menu = Menu("File")

            val closeMenuItem = MenuItem("Close")
            closeMenuItem.onAction = EventHandler { view.close() }
            menu.items.addAll(closeMenuItem)
            return menu
        }
    }
}
package eu.khaos.studio.fx

import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.cdi.MvvmfxCdiApplication
import eu.khaos.studio.fx.main.MainView
import javafx.application.Application
import javafx.scene.Scene
import javafx.stage.Stage

fun main(args: Array<String>) {
    Application.launch(App::class.java)
}

class App : MvvmfxCdiApplication() {
    override fun startMvvmfx(stage: Stage) {

        stage.title = "Khaos Studio"

        val main = FluentViewLoader.javaView(MainView::class.java).load()
        stage.scene = Scene(main.getView())
        stage.show()
    }

}
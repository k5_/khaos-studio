package eu.k5.arch

import javafx.scene.Scene
import javafx.scene.Parent
import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.ViewTuple
import javafx.application.Application
import javafx.stage.Stage

fun main(args: Array<String>) {
    Application.launch(Starter::class.java as Class<out Application>)
}

class Starter : Application() {
    @Throws(Exception::class)
    override fun start(stage: Stage) {
        stage.setTitle("Hello World Application")

        val viewTuple = FluentViewLoader.javaView(HelloWorldView::class.java).load()

        val root = viewTuple.view
        stage.setScene(Scene(root))
        stage.show()
    }
}
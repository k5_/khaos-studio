package eu.k5.arch

import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.InjectViewModel
import de.saxsys.mvvmfx.JavaView
import eu.k5.arch.menu.MenuView
import javafx.scene.layout.VBox


class MainView : VBox(), JavaView<MainViewModel> {

    @InjectViewModel
    var model: MainViewModel? = null

    init {

    }

    fun initialize() {

        val menuView = FluentViewLoader.javaView(MenuView::class.java).load()
        children.add(0, menuView.view)


    }

}
package eu.khaos.storage.file.boundary

import eu.khaos.storage.*
import eu.khaos.storage.file.control.FileSanitizer
import eu.khaos.storage.file.control.FileModel
import eu.khaos.storage.file.model.FileFolder
import eu.khaos.storage.file.model.FileProject
import eu.khaos.storage.file.model.FileProjectRef
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.nio.file.FileVisitResult
import java.nio.file.FileVisitor
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes
import java.util.*
import java.util.regex.Pattern
import javax.ws.rs.core.Response


class RepositoryResourceBean(
        val path: Path
) : RepositoryResource {

    private val sanitizer = FileSanitizer()
    private val model = FileModel()

    override fun list(): ProjectTree {
        //    val tree = ProjectTree()
        //  Files.walkFileTree(path, ProjectTreeVisitor(tree, model))
        //return tree
        TODO("removed")
    }


    override fun createProject(createProject: CreateProject): Project {

        val projectId = sanitizer.sanitize(createProject.name!!)

        if (createProject.parent != null) {

        }

        val projectPath = resolvePojectPath(projectId)

        if (Files.exists(projectPath)) {
            throw ConsumerException("Already existing Project", Response.Status.BAD_REQUEST)
        }

        Files.createDirectories(projectPath)

        val projectFile = projectPath.resolve("storage.xml")

        val fileProject = FileProject()
        fileProject.name = createProject.name


        model.toPath(fileProject, projectFile)

        return project(projectId).get()
    }

    override fun project(projectId: String): ProjectResource {
        val filePath = resolvePojectPath(projectId).resolve("storage.xml")
        return ProjectResouceBean(FileProjectRef(self = "/storage/" + projectId, projectId = projectId, filePath = filePath), this)
    }


    fun resolvePojectPath(uri: String): Path {
        if (!uriPattern.matcher(uri).matches()) {
            println(uri)
            throw ConsumerException("Invalid URI", Response.Status.BAD_REQUEST);
        }
        return path.resolve(uri)
    }


    companion object {
        private val uriPattern = Pattern.compile("[a-z]{3,10}")

    }
}